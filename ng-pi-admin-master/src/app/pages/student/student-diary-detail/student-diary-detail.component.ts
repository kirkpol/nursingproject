import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../service/session.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StudentDiaryDetailService } from './student-diary-detail.service';
import { DiaryDetailParaModel } from '../../../models/diary/diary-Para.model';
import { DiaryDetailModel, DiaryHashtagDetail } from '../../../models/diary/diary-detail.model';
import { CommentModel } from '../../../models/diary/diary-comment.model';
import { HashTagModel, DiaryCreateModel } from '../../../models/diary/diary-create.model';
import swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-student-diary-detail',
  templateUrl: './student-diary-detail.component.html',
  styleUrls: ['./student-diary-detail.component.scss']
})
export class StudentDiaryDetailComponent implements OnInit {

  diaryDetailForm: FormGroup;
  diaryPara: DiaryDetailParaModel = new DiaryDetailParaModel();
  diaryDetail: DiaryDetailModel = new DiaryDetailModel();
  diaryUpdate: DiaryCreateModel = new DiaryCreateModel();
  successDiaryPara: DiaryDetailParaModel = new DiaryDetailParaModel();
  comment: Array<CommentModel> = new Array<CommentModel>();
  tag: Array<DiaryHashtagDetail> = new Array<DiaryHashtagDetail>();
  showtag: Array<string> = new Array<string>();
  ServiceType: Array<string> = new Array<string>();
  data: string;
  tagValue: string;
  hashTagList: Array<HashTagModel> = new Array<HashTagModel>();
  hidebutton: any[] = [];
  dateShow: Date;
  dateSendingToServer: string;
  constructor(
    private sessionService: SessionService,
    private studentDiaryDetailService: StudentDiaryDetailService,
    private formBuilder: FormBuilder
  ) {
    this.diaryDetailForm = formBuilder.group({
      title: [""],
      dateward: [""],
      content: [""],
      tag: [""]
    });
  }

  ngOnInit() {
    this.diaryPara.diaryId = this.sessionService.getItemFromStorage("diaryId")
    this.getDiaryDetail();
    this.getComment();
  }
  getDiaryDetail() {
    this.studentDiaryDetailService.getDiaryDetail(this.diaryPara).subscribe(
      response => {
        this.diaryDetail = response
        this.tag = this.diaryDetail.diaryHashtagDetails
        console.log("Diary", this.diaryDetail)
        this.dateSendingToServer = new DatePipe('en-US').transform(this.diaryDetail.dateWard, 'MM/dd/yyyy')
        console.log("date", this.dateSendingToServer)
        this.getkeyfromtagjson()
      },
      error => {
      }
    );
  }

  getComment() {
    this.studentDiaryDetailService.getCommentDetail(this.diaryPara).subscribe(
      response => {
        this.comment = response
        console.log("Comment", this.comment)
      },
      error => {
      }
    );
  }

  updateDiary() {
    this.diaryUpdate.diaryTitle = this.diaryDetail.diaryTitle
    this.diaryUpdate.diaryContent = this.diaryDetail.diaryContent
    this.diaryUpdate.studentId = this.diaryDetail.studentId
    this.diaryUpdate.subjectId = this.diaryDetail.subjectId
    this.diaryUpdate.HashTagList = this.hashTagList
    this.diaryUpdate.dateWard = this.diaryDetail.dateWard
    console.log(this.diaryUpdate)
  }

  getkeyfromtagjson(): void {
    for (const i in this.tag) {
      this.showtag[i] = this.tag[i].hashTagName;
      this.ServiceType.push(this.tag[i].hashTagName)
    }
  }

  successDiary() {

    swal({
      title: 'ต้องการส่งไดอารี่',
      text: 'กรุณาตรวจสอบให้แน่นอน',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ตกลง',
      cancelButtonText: "ยกเลิก"
    }).then((result) => {
      if (result.value == true) {
        this.successDiaryPara.diaryId = this.diaryDetail.diaryId
        this.studentDiaryDetailService.SuccessDiary(this.successDiaryPara).subscribe(
          response => {
            swal({
              position: 'center',
              type: 'success',
              title: 'ไดอารี่ส่งเรียบร้อยแล้ว',
              showConfirmButton: false,
              timer: 1500
            }).then(() => {
              location.reload();
            })
          })
      }
      else {
        console.log("ผิดพลาด")
      }
    })
  }

  addtag(productId: number, name: string): void {
    this.data = this.diaryDetailForm.controls['tag'].value;
    if (this.data == undefined || this.data.toString().trim().length == 0) {
      swal({
        position: 'center',
        type: 'warning',
        title: 'กรุณาระบุคำที่น่าสนใจ',
        showConfirmButton: false,
        timer: 1000
      })
    } else {
      if (this.ServiceType.length != 3) {
        if (productId === 0) {
          this.ServiceType.push(name);
          this.hashTagList.push({ hashTagName: name })
          this.tagValue = "";
        }
        else {
          this.hidebutton[productId] = true;
          this.ServiceType.push(name);
        }
      } else {
        this.diaryDetailForm.controls['tag'].disable();
      }
    }
  }

  removetag(name: string): void {
    for (let i = this.ServiceType.length - 1; i >= 0; i--) {
      if (this.ServiceType[i] === name) {
        this.ServiceType.splice(i, 1);
        console.log(this.ServiceType.toString());
        // break;       //<-- Uncomment  if only the first term has to be removed
      }
      if (this.ServiceType.length < 3) {
        this.diaryDetailForm.controls['tag'].enable();
      }
    }
  }
}
