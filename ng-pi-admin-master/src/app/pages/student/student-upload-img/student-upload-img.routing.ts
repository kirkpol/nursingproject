import { Routes, RouterModule } from '@angular/router';
import { StudentUploadImgComponent } from './student-upload-img.component';

const routes: Routes = [
  { 
    path:'',
    component:StudentUploadImgComponent
   },
];

export const StudentUploadImgRoutes = RouterModule.forChild(routes);
