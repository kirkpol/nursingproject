import { Component, OnInit } from '@angular/core';
import { StudentUploadImgService } from './student-upload-img.service';
import { DiaryCreateModel } from '../../../models/diary/diary-create.model';

class ImageSnippet {
  constructor(public src: string, public file: File) {}
}


@Component({
  selector: 'app-student-upload-img',
  templateUrl: './student-upload-img.component.html',
  styleUrls: ['./student-upload-img.component.scss']
})
export class StudentUploadImgComponent implements OnInit {
  selectedFile: ImageSnippet;
  file:File;
  model :DiaryCreateModel = new DiaryCreateModel()
  constructor(
    private StudentUploadImgService: StudentUploadImgService
    ) { }

  ngOnInit() {
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {

      this.selectedFile = new ImageSnippet(event.target.result, file);
      this.model.diaryTitle = "1"
      this.model.diaryContent ="2"
      this.model.studentId = "580107030013"
      this.model.subjectId = 50
      this.model.uploadImage = this.selectedFile.file
      console.log(this.selectedFile.file)
      this.StudentUploadImgService.uploadImage(this.selectedFile.file).subscribe(
        (res) => {
        console.log(res)
        },
        (err) => {
        
        })
    });

    reader.readAsDataURL(file);
  }
}
