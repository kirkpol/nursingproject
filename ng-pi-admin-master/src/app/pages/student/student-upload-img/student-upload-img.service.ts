import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Router } from '@angular/router';
import { isMaster } from 'cluster';
import { DiaryCreateModel } from '../../../models/diary/diary-create.model';

@Injectable()
export class StudentUploadImgService {

    constructor(
        private http: HttpClient,
        private router: Router
    ) { }
    sub: any;

     uploadImage(image: File): Observable<Response> {
      const url = environment.url + "api/Upload/Upload";
      const formData = new FormData();
    
      formData.append('image', image ); 
    
    this.sub = this.http.post<any>(url, formData);
    return this.sub;
    }

    recordDiary(diarydata: DiaryCreateModel): Observable<any> {
        const url = environment.url + "api/Diary/RecordDiary";
        const headers = new HttpHeaders({
            "Content-Type": "text/json"
        });
        this.sub = this.http.post<any>(url, diarydata, { headers });

        return this.sub;
    }
}

