import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentUploadImgComponent } from './student-upload-img.component';
import { SharedModule } from '../../../shared/shared.module';
import { ModalModule } from 'ngx-modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentUploadImgService } from './student-upload-img.service';
import { StudentUploadImgRoutes } from './student-upload-img.routing';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    StudentUploadImgRoutes,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: 
  [
    StudentUploadImgComponent
  ],
  providers: [
    StudentUploadImgService
  ]
})
export class StudentUploadImgModule { }
