import { Component, OnInit } from '@angular/core';
import { DiaryCreateModel, HashTagModel } from '../../../models/diary/diary-create.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { ServiceBuilder } from 'selenium-webdriver/edge';
import { SessionService } from '../../../service/session.service';
import { StudentDiaryCreateService } from './student-diary-create.service';
import { SubjectDetailModel } from '../../../models/subject/subject-detail.model';
import { SujectDetailParaModel } from '../../../models/subject/subject-para.model';

@Component({
  selector: 'app-student-diary-create',
  templateUrl: './student-diary-create.component.html',
  styleUrls: ['./student-diary-create.component.scss']
})
export class StudentDiaryCreateComponent implements OnInit {
  diaryCreateData: DiaryCreateModel = new DiaryCreateModel();
  ServiceType: Array<string> = new Array<string>();
  hashTagList: Array<HashTagModel> = new Array<HashTagModel>();
  diaryDetailPara : SujectDetailParaModel = new SujectDetailParaModel();
  subjectDetail : SubjectDetailModel = new SubjectDetailModel();
  tagValue: string;
  diaryCreateForm: FormGroup;
  hidebutton: any[] = [];
  data: string;
  i: number;
  constructor(
    private sessionService: SessionService,
    private diaryService: StudentDiaryCreateService,
    private formBuilder: FormBuilder
  ) {
    this.diaryCreateForm = formBuilder.group({
      title: [""],
      dateward: [""],
      content: [""],
      tag: [""]

    });
  }

  ngOnInit() {
    this.getSubjectDetail();
  }

  CreateDiary(): void {
    this.diaryCreateData.HashTagList = this.hashTagList
    this.diaryCreateData.studentId = this.sessionService.getItemFromStorage("userId")
    this.diaryCreateData.subjectId = this.sessionService.getItemFromStorage("subjectId")
    console.log(this.diaryCreateData)
    this.diaryService.recordDiary(this.diaryCreateData)
    .subscribe(
      response => {     
        if(response == true){
          swal({
            position: 'center',
            type: 'success',
            title: 'สร้างไดอารี่เรียบร้อย',
            showConfirmButton: false,
            timer: 1000
          }).then(() => {           
             location.reload();            
         })
        } else{
          swal({
            position: 'center',
            type: 'warning',
            title: 'เกิดข้อผิดพลาด',
            showConfirmButton: false,
            timer: 1000
          }).then(() => {           
             location.reload();            
         })
        }
      },
       error => {
      }
    );
  }

  getSubjectDetail(){
    this.diaryDetailPara.subjectId =this.sessionService.getItemFromStorage("subjectId")
    this.diaryService.getSubjectDetail(this.diaryDetailPara)
    .subscribe(
      response => {   
        this.subjectDetail = response  
        console.log(this.subjectDetail)
      },
       error => {
      }
    );
  }

  addtag(productId: number, name: string): void {
    this.data = this.diaryCreateForm.controls['tag'].value;
    if (this.data == undefined || this.data.toString().trim().length == 0) {
      swal({
        position: 'center',
        type: 'warning',
        title: 'กรุณาระบุคำที่น่าสนใจ',
        showConfirmButton: false,
        timer: 1000
      })
    } else {
      if (this.ServiceType.length != 3) {
        if (productId === 0) {
          this.ServiceType.push(name);
          this.hashTagList.push({ hashTagName: name })
          this.tagValue = "";
        }
        else {
          this.hidebutton[productId] = true;
          this.ServiceType.push(name);
        }
      } else {
        this.diaryCreateForm.controls['tag'].disable();
      }
    }
  }

  removetag(name: string): void {
    for (let i = this.ServiceType.length - 1; i >= 0; i--) {
      if (this.ServiceType[i] === name) {
        this.ServiceType.splice(i, 1);
        console.log(this.ServiceType.toString());
        // break;       //<-- Uncomment  if only the first term has to be removed
      }
      if (this.ServiceType.length < 3) {
        this.diaryCreateForm.controls['tag'].enable();
      }
    }
  }
}
