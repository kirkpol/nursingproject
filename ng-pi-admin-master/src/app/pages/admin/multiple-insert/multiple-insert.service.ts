import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { MultipleInsertModel } from '../../../models/admin/insert.model';
import { ContentType } from '@angular/http/src/enums';

@Injectable()
export class MultipleInsertService {

    constructor(
        private http: HttpClient,
        private router: Router
    ) { }
    sub: any;

    public UploadExcel(multiple:MultipleInsertModel): Observable<any> {
      const url = environment.url + "api/Upload/Excel";
      const formData = new FormData();
     
      formData.append('file', multiple.file );   
      formData.append('userType', multiple.userType.toString());      
      
      this.sub = this.http.post<any>(url, formData );   
    return this.sub;
    }
   

}
