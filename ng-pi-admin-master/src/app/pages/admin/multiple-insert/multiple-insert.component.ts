import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { MultipleInsertService } from './multiple-insert.service';
import { MultipleInsertModel } from '../../../models/admin/insert.model';
import swal from 'sweetalert2';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-multiple-insert',
  templateUrl: './multiple-insert.component.html',
  styleUrls: ['./multiple-insert.component.scss']
})
export class MultipleInsertComponent implements OnInit {
  multipleInsert: FormGroup;
  multiple:MultipleInsertModel= new MultipleInsertModel();
  userType:number = 0;
  file:File;
  name :string = "none";
  constructor(
    private formBuilder: FormBuilder,
    private multipleInsertService: MultipleInsertService
  ) {
    this.multipleInsert = formBuilder.group({     
      type: ["กรุณาเลือกประเภท", [Validators.required, Validators.nullValidator]]
    });
   }

  ngOnInit() {
  }

  incomingfile(event) 
  {
  this.file= event.target.files[0]; 
  this.name = this.file.name;
  console.log(this.file)
  console.log(this.name)
  }
  
  Ok(){
    this.multiple.file = this.file;
    this.multiple.userType =  this.userType;
    console.log(this.multiple)

    if(this.multiple.file === undefined){
      swal({
        position: 'center',
        type: 'warning',
        title: 'เลือกไฟล์ Excel',
        showConfirmButton: false,
        timer: 2000
      }) 
    }else if (this.multiple.userType == 0)
    {
      swal({
        position: 'center',
        type: 'warning',
        title: 'กรุณากรอกเลือกประเภทข้อมูล',
        showConfirmButton: false,
        timer: 2000
      }) 
    }else{
      console.log(this.userType)
       this.multipleInsertService.UploadExcel(this.multiple).subscribe(       
      (res) => {
      console.log(res)
      },
      (err) => {
      
      })
    }
    
    
  }

  

}
