
export class DiaryDetailModel {
    diaryId: number;
    diaryTitle: string;
    diaryContent: string;
    dateCreateToString: string;
    dateWardToString: string;
    dateSend: Date;
    dateWard: Date;
    dateCreate: Date;
    dateUpdate: Date;
    statusId: number;
    studentId: string;
    subjectId: number;
    subjectName: string;
    teacherId: string;
    diaryPictureDetials: Array<DiaryPictureModel> = new Array<DiaryPictureModel>();
    diaryHashtagDetails: Array<DiaryHashtagDetail> = new Array<DiaryHashtagDetail>();
}

export class DiaryHashtagDetail {
    hashTagId: number;
    hashTagName: string;
}

export class DiaryPictureModel {
    pidId: number;
    picName: string;
}


