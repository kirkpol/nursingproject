export class DiaryCreateModel{
    diaryTitle: string;
    diaryContent: string;
    dateCreate: Date;
    dateWard: Date;
    studentId: string;
    subjectId : number;
    HashTagList: Array<HashTagModel> = new Array<HashTagModel>();
    uploadImage: File;
}

export class HashTagModel{
    hashTagName: string; 
}