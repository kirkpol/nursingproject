export class InsertModel{
    userId: string;
    password: string;
    firstname: string;
    lastname: string;
    typeId:number;
}

export class MultipleInsertModel{
    file:File;
    userType:number;
}