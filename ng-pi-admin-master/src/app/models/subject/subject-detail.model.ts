export class SubjectDetailModel{
    subjectId: number;
    subjectName: string;
    qrCode: string;
    teacherCreateId: string;
    teacherCreateName: string;
    teacherCreateType: string;
    teacherAssistants: Array<TeacherAssistantModel> = new Array<TeacherAssistantModel>();
}

export class TeacherAssistantModel{
    teacherId: string;
    teacherName: string;
    teacherType: string;
}
