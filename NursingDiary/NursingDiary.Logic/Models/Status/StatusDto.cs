﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Status
{
    public class StatusDto
    {
        public int StatusType { get; set; }
        public string StatusName { get; set; }
    }
}
