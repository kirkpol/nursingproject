﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Subject
{
    public class SubjectDetailDto
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string TeacherId { get; set; }
        public string TeacherHeadName { get; set; }
        public string QRCode { get; set; }
        public string QRCodePath { get; set; }
        public List<TeacherAssistantDetailDto> TeacherAssistants { get; set; }
    }

    public class TeacherAssistantDetailDto
    {
        public string TeacherAssistantName { get; set; }
        public string TeacherId { get; set; }
    }


}
