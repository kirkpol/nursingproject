﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Subject
{
    public class SubjectIndexListDto
    {
        public int SubjectId { get; set; }
        public string StudentId { get; set; }
        public string SubjectName { get; set; }
        public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherType { get; set; }
    }
}

