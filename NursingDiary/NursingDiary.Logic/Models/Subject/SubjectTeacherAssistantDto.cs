﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Subject
{
    class SubjectTeacherAssistantDto
    {
        public int SubjectId { get; set; }
        public string TeacherId { get; set; }
        public string TeacherType { get; set; }
    }
}
