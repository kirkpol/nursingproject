﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Subject
{
   public class SubjectSelectorDto
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string QRCode { get; set; }
        public string QRCodePath { get; set; }
        public string TeacherCreateId { get; set; }
        public string TeacherCreateName { get; set; }
        public string TeacherCreateType { get; set; }
        public List<TeacherAssistantSelectorDto> AssistantSelector { get; set; }
        public List<StudenSubjectDetailDto> StudenSubjectDetail { get; set; }
    }

    public class TeacherAssistantSelectorDto
    {
        public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherType { get; set; }
    }

    public class StudenSubjectDetailDto
    {
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string TeacherId { get; set; }
    }
}
