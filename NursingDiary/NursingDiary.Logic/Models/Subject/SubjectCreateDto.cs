﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Subject
{
    public class SubjectCreateDto
    {
        public string SubjectName { get; set; }
        public string TeacherId { get; set; }
        public string QRCode { get; set; }
        public List<TeacherAssistantDto> TeacherAssistants { get; set; }
    }

    public class TeacherAssistantDto
    {
        public string TeacherId { get; set; }
    }
}
