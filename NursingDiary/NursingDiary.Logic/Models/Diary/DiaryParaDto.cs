﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Diary
{
   public class DiaryParaDto
    {
        public string StudentId { get; set; }
        public string TeacherId { get; set; }
        public int SubjectId { get; set; }
    }
}
