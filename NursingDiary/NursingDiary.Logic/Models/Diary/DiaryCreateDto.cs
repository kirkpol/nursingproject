﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Diary
{
    public class DiaryCreateDto
    {
        public string DiaryTitle { get; set; }
        public string DiaryContent { get; set; }
        public DateTime DateCreate { get; set; }  // วันที่สร้างรับค่าจากผู้ใช้
        public DateTime DateWard { get; set; }    // เวลาขึ้น Ward    
        public int? StatusId { get; set; }
        public string StudentId { get; set; }
        public int SubjectId { get; set; }
        public DiaryPictureCreateDto PictureList { get; set; }
        public List<DiaryHashTagCrateDto> HashTagList { get; set; }
    }

    public class DiaryPictureCreateDto
    {
        public int PicId { get; set; }
        public string PicName { get; set; }
    }
    public class DiaryHashTagCrateDto
    {
        public int HashTagId { get; set; }
        public string HashTagName { get; set; }
    }
}
