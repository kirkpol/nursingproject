﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Diary
{
   public class HasgtagDto
    {
        public int HashTagId { get; set; }
        public string HashTagName { get; set; }
    }
}
