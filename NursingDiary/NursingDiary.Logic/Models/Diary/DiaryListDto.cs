﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Diary
{
   public class DiaryListDto
    {
        public int DiaryId { get; set; }
        public int StatusId { get; set; }
        public int SubjectId { get; set; }
        public string subjectname { get; set; }
        public string DiaryTitle { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string DateWard { get; set; }
    }
}
