﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Diary
{
   public class PictureDto
    {
        public int PicId { get; set; }
        public string PicName { get; set; }
    }
}
