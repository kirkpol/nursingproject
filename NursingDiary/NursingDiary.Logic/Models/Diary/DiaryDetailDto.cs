﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Diary
{
    public class DiaryDetailDto
    {
        public int? DiaryId { get; set; }
        public string DiaryTitle { get; set; }
        public string DiaryContent { get; set; }
        public string DateSend { get; set; }
        public string DateWardToString { get; set; }
        public string DateCreateToString { get; set; }
        public DateTime DateWard { get; set; }
        public DateTime DateCreate { get; set; }
        public string DateUpdate { get; set; } // Auto เมื่อมาแก้ไข
        public int? StatusId { get; set; }
        public string StudentId { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string TeacherId { get; set; }
        public List<PictureDto> diaryPictureDetials { get; set; }
        public List<HasgtagDto> diaryHashtagDetails { get; set; }      
    }
}
