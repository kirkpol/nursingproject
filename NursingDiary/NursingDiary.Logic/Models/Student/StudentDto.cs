﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Student
{
    public class StudentDto
    {
        public string StudentId { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
    }
}
