﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.User
{
   public class UserDto
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int TypeId { get; set; }
    }
}
