﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Upload
{
   public class UploadPhotoDto
    {
        public bool Success { get; set; } 
        public string FileName { get; set; }
    }
}
