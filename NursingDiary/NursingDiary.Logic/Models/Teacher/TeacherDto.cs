﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Teacher
{
    public class TeacherDto
    {
        public string TeacherId { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
       /* public int CommentCount { get; set; }
        public int Group { get; set; }
        public int subject { get; set; }
        public int TeacherHelp { get; set; }*/
    }
}
