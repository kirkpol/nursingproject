﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Comment
{
   public class CommentDto
    {
        public int CommentId { get; set; }
        public int DiaryId { get; set; }
        public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string CommentContent { get; set; }
        public DateTime DateComment { get; set; }
    }
}
