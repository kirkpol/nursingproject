﻿using NursingDiary.Logic.Models.Status;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Interface
{
    public interface IStatusService
    {
        Task<bool> CreateStatusAsync(StatusDto model);
    }
}
