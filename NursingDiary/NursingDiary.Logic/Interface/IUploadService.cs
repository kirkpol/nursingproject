﻿using Microsoft.AspNetCore.Http;
using NursingDiary.Logic.Models.Upload;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Interface
{
    public interface IUploadService
    {
        Task<bool> ImportExcelFileAsync(IFormFile excel , int userType);
        Task<UploadPhotoDto> UploadObjectAsync(IFormFile image);
    }
}
