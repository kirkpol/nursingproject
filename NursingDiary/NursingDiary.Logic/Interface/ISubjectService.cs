﻿using NursingDiary.Logic.Models.Subject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Interface
{
    public interface ISubjectService
    {
        Task<SubjectDetailDto> CreateSubjectAsync(SubjectCreateDto model);
        Task<SubjectSelectorDto> SubjectDetailAsync(int subjectId);
        Task<bool> AddSubjectAsync(string studentId, string teacherId, string qrcode);
        Task<List<SubjectIndexListDto>> SubjectTeacherListAsync(string teacherId);
        Task<List<SubjectIndexListDto>> SubjectStudentListAsync(string studentId);
        Task<SubjectSelectorDto> SubjectSelectorAsync(string QRCode);
    }
}
