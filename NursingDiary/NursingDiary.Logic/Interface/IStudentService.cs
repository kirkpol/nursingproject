﻿using NursingDiary.Logic.Models.Student;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Interface
{
    public interface IStudentService
    {
        Task<bool>InsertStudentAsync(StudentDto model);
        Task<StudentDto> GetStudentDetailAsync(string id);
        Task<bool> DeleteStudentAsync(string id);
        Task<List<StudentDto>> GetAllStudentAsync();
        Task<bool> EditStudentAsync(StudentDto model);
    }
}
