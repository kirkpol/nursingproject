﻿using NursingDiary.Logic.Models.Diary;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Interface
{
    public interface IDiaryService
    {
        Task<bool> RecordDiaryAsync(DiaryCreateDto model);
        Task<bool> SuccessDiaryAsyncs(int diaryId);
        Task<DiaryDetailDto> DiaryDetailAsync(int diaryId) ;
        Task<bool> DiaryEditeAsync(DiaryDetailDto model);
        Task<List<DiaryListDto>> GetAllDiaryListAsync(string studentId);
        Task<List<DiaryListDto>> DiaryTeacherListAsync(DiaryParaDto model);
        Task<List<DiaryListDto>> DiaryStudentListAsync(DiaryParaDto model);
    }
}
