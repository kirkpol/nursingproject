﻿using NursingDiary.Logic.Models.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Interface
{
    public interface IUserService
    {
        Task<UserDto> LoginAsync(string Username , string password);
    }
}
