﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Interface
{
    public interface ITrippleDESAlgorithmService
    {
        Task<string> EncryptAsync(string EncryptPassword);
        Task<string> DecryptAsync(string DecryptPassword);
    }
}
