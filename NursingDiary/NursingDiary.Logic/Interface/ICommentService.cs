﻿using NursingDiary.Logic.Models.Comment;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace NursingDiary.Logic.Interface
{
    public interface ICommentService
    {
        Task<CommentDto> CreateCommentAsync(CommentDto model);
        Task<List<CommentDetailDto>> CommentDetailAsync(int diaryId);
    }
}
