﻿using Microsoft.AspNetCore.Http;
using NursingDiary.Logic.Models.Teacher;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Interface
{
    public interface ITeacherService
    {
        Task<bool>InsertTeacherAsync(TeacherDto model);
        Task<TeacherDto>GetTeacherDetailAsync(string id);
        Task<List<TeacherDto>>GetAllTeacherAsync();
        Task<bool> DeleteTeacherAsync(string id);
        Task<bool> EditTeacherAsync(TeacherDto model);
        //Task<bool> AttachTeacherExcelAsync(IFormFile file);
    }
}
