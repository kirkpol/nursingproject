﻿using Microsoft.EntityFrameworkCore;
using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Status;
using NursingDiary.Logic.Models.Subject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QRCoder;
using System.IO;
using System.Drawing;
using System.Net;
using ZXing.QrCode;
using Amazon.S3;
using Amazon.S3.Model;
using NursingDiary.Logic.Models.Upload;
using Microsoft.AspNetCore.Http;

namespace NursingDiary.Logic.Implement
{
    public class SubjectService : ISubjectService
    {
        private IEntityUnitOfWork EntityUnitOfWork { get; set; }
        private static Random random = new Random();
        public SubjectService(IEntityUnitOfWork EntityUnitOfWork)
        {
            this.EntityUnitOfWork = EntityUnitOfWork;
        }

        public async Task<SubjectDetailDto> CreateSubjectAsync(SubjectCreateDto model)
        {
            try
            {
                string qrcodeRandom = "";
                var checkRepeat = true;
                do
                {
                    qrcodeRandom = RandomString();
                    checkRepeat = await CheckQrCodeList(qrcodeRandom);
                } while (checkRepeat == false);

                var subjectData = new Subject()
                {
                    ClassName = model.SubjectName,
                    TeacherId = model.TeacherId,
                    Qrcode = qrcodeRandom
                };
                await EntityUnitOfWork.SubjectRepository.AddAsync(subjectData);

                var teacherHeader = new TeacherHelper()
                {
                    ClassId = subjectData.ClassId,
                    TeacherId = model.TeacherId,
                    TeacherType = "Master"
                };
                await EntityUnitOfWork.TeacherHelperRepository.AddAsync(teacherHeader);

                if (model.TeacherAssistants != null)
                {
                    var teacherAssistants = model.TeacherAssistants.Select(item => new TeacherHelper()
                    {
                        ClassId = subjectData.ClassId,
                        TeacherId = item.TeacherId,
                        TeacherType = "Assistants"
                    }).ToList();

                    await EntityUnitOfWork.TeacherHelperRepository.AddAsync(teacherAssistants);
                    await EntityUnitOfWork.SaveAsync();
                }
                await EntityUnitOfWork.SaveAsync();
                      
                var QRCodeImg = QRCodeImgAsync(qrcodeRandom).Result;
                subjectData.QrcodePath = "https://s3-ap-southeast-1.amazonaws.com/nursing-diary-store-s3/QRCode/"+ QRCodeImg.FileName;
                await EntityUnitOfWork.SaveAsync();

                var SubjectDetail = new SubjectDetailDto()
                {
                    SubjectId = subjectData.ClassId,
                    SubjectName = subjectData.ClassName,
                    QRCode = subjectData.Qrcode,
                    QRCodePath = subjectData.QrcodePath
                };

                return SubjectDetail;
            }
            catch (Exception e)
            {
                return new SubjectDetailDto();
            }


        }

        public async Task<SubjectSelectorDto> SubjectDetailAsync(int subjectId)
        {
            try
            {
                var subjectData = await EntityUnitOfWork.SubjectRepository.GetSingleAsync(w => w.ClassId == subjectId);

                var result = new SubjectSelectorDto()
                {
                    SubjectId = subjectData.ClassId,
                    SubjectName = subjectData.ClassName,
                    QRCode = subjectData.Qrcode,
                    QRCodePath = subjectData.QrcodePath,
                    TeacherCreateId = subjectData.TeacherId,
                    TeacherCreateName = EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == subjectData.TeacherId).Result.Firstname + " " + EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == subjectData.TeacherId).Result.Lastname,
                    TeacherCreateType = ConvertType(EntityUnitOfWork.TeacherHelperRepository.GetSingleAsync(w => w.TeacherId == subjectData.TeacherId && w.ClassId == subjectData.ClassId).Result.TeacherType)
                };

                var assistant = await EntityUnitOfWork.TeacherHelperRepository.GetAll().Where(w => w.ClassId == subjectData.ClassId).ToListAsync();

                var resTea = assistant.Select(s => new TeacherAssistantSelectorDto()
                {
                    TeacherId = s.TeacherId,
                    TeacherName = EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Firstname + " " + EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Lastname,
                    TeacherType = ConvertType(s.TeacherType)
                }).ToList();

                var student = await EntityUnitOfWork.GroupRepository.GetAll().Where(w => w.ClassId == subjectData.ClassId).ToListAsync();

                var resStu = student.Select(s => new StudenSubjectDetailDto()
                {
                    StudentId = s.StudentId,
                    StudentName = EntityUnitOfWork.StudentRepository.GetSingleAsync(w => w.StudentId == s.StudentId).Result.Firstname + " " + EntityUnitOfWork.StudentRepository.GetSingleAsync(w => w.StudentId == s.StudentId).Result.Lastname,
                    TeacherId = s.TeacherId
                }).ToList();


                result.AssistantSelector = resTea;
                result.StudenSubjectDetail = resStu;

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<bool> AddSubjectAsync(string studentId, string teacherId, string qrcode)
        {
            try
            {       
                // ข้อมูลวิชา
                var qrcodedata = await EntityUnitOfWork.SubjectRepository.GetSingleAsync(g => g.Qrcode == qrcode);
                // ข้อมูลอาจารย์
                var subjectdata = await EntityUnitOfWork.TeacherHelperRepository.GetSingleAsync(g => g.ClassId == qrcodedata.ClassId && g.TeacherId == teacherId);
                // เช็คข้อมูลที่ซ้ำ
                var checkRepeat = await EntityUnitOfWork.GroupRepository.GetSingleAsync(w => w.StudentId == studentId && w.TeacherId == teacherId && w.ClassId == qrcodedata.ClassId);
                
                if(checkRepeat == null)
                {
                    var group = new Group()
                    {
                        ClassId = qrcodedata.ClassId,
                        StudentId = studentId,
                        TeacherId = subjectdata.TeacherId,
                        TeacherType = subjectdata.TeacherType
                    };

                    await EntityUnitOfWork.GroupRepository.AddAsync(group);
                    await EntityUnitOfWork.SaveAsync();
                    return true;
                }
                else
                {
                    return false;
                }
             
                //var teacherMasterdata = await EntityUnitOfWork.TeacherRepository.GetSingleAsync(g => g.TeacherId == qrcodedata.TeacherId);
                //var teacherAssistantsdata = await EntityUnitOfWork.TeacherRepository.GetSingleAsync(g => g.TeacherId == subjectdata.TeacherId);

                //var detaildata = new SubjectSingleDetailDto()
                //{
                //    SubjectName = qrcodedata.ClassName,
                //    TeacherHeadId = teacherMasterdata.TeacherId,
                //    TeacherHeadName = teacherMasterdata.Firstname + " " + teacherMasterdata.Lastname,
                //    TeacherAssistantId = teacherAssistantsdata.TeacherId,
                //    TeacherAssistantName = teacherAssistantsdata.Firstname + " " + teacherAssistantsdata.Lastname,
                //    QRCode = qrcodedata.Qrcode
                //};
                //return detaildata;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<List<SubjectIndexListDto>> SubjectTeacherListAsync(string teacherId)
        {
            try
            {
                var subjectData = await EntityUnitOfWork.TeacherHelperRepository.GetAll(w => w.TeacherId == teacherId).ToListAsync();

                var result = subjectData.Select(s => new SubjectIndexListDto()
                {
                    SubjectId = (int)s.ClassId,
                    SubjectName = EntityUnitOfWork.SubjectRepository.GetSingleAsync(w => w.ClassId == s.ClassId).Result.ClassName,
                    TeacherId = s.TeacherId,
                    TeacherName = EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Firstname + " " + EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Lastname,
                    TeacherType = ConvertType(s.TeacherType)
                }).ToList();

                return result;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<List<SubjectIndexListDto>> SubjectStudentListAsync(string studentId)
        {
            try
            {
                var subjectData = await EntityUnitOfWork.GroupRepository.GetAll(w => w.StudentId == studentId).ToListAsync();

                var result = subjectData.Select(s => new SubjectIndexListDto()
                {
                    SubjectId = (int)s.ClassId,
                    SubjectName = EntityUnitOfWork.SubjectRepository.GetSingleAsync(w => w.ClassId == s.ClassId).Result.ClassName,
                    StudentId = s.StudentId,
                    TeacherId = s.TeacherId,
                    TeacherName = EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Firstname + " " + EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Lastname,
                    TeacherType = ConvertType(s.TeacherType)
                }).ToList();
                return result;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<SubjectSelectorDto> SubjectSelectorAsync(string QRCode)
        {
            try
            {
                var subjectData = await EntityUnitOfWork.SubjectRepository.GetSingleAsync(w => w.Qrcode == QRCode);

                var result = new SubjectSelectorDto()
                {
                    SubjectId = subjectData.ClassId,
                    SubjectName = subjectData.ClassName,
                    QRCode = subjectData.Qrcode,
                    QRCodePath = subjectData.QrcodePath,
                    TeacherCreateId = subjectData.TeacherId,
                    TeacherCreateName = EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == subjectData.TeacherId).Result.Firstname + " " + EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == subjectData.TeacherId).Result.Lastname,
                    TeacherCreateType = ConvertType(EntityUnitOfWork.TeacherHelperRepository.GetSingleAsync(w => w.TeacherId == subjectData.TeacherId && w.ClassId == subjectData.ClassId).Result.TeacherType)
                };

                var assistant = await EntityUnitOfWork.TeacherHelperRepository.GetAll().Where(w => w.ClassId == subjectData.ClassId).ToListAsync();

                var res = assistant.Select(s => new TeacherAssistantSelectorDto()
                {
                    TeacherId = s.TeacherId,
                    TeacherName = EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Firstname + " " + EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Lastname,
                    TeacherType = ConvertType(s.TeacherType)
                }).ToList();

                result.AssistantSelector = res;

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private string ConvertType(string teacherType)
        {
            string data = null;
            if (teacherType == "Master")
            {
                data = "หัวหน้าวิชา";
            }
            else if (teacherType == "Assistants")
            {
                data = "ผู้ช่วย";
            }

            return data;
        }

        private static string RandomString()
        {
            const string chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
            return new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(10)]).ToArray());
        }

        private async Task<bool> CheckQrCodeList(string qrcodeRandom)
        {
            var qrcodeCheck = await EntityUnitOfWork.SubjectRepository.GetSingleAsync(g => g.Qrcode == qrcodeRandom);

            if (qrcodeCheck != null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private async Task<UploadPhotoDto> QRCodeImgAsync(string qrcode)
        {

            var QrcodeContent = qrcode;
            var alt = qrcode;
            var width = 500; // width of the Qr Code   
            var height = 500; // height of the Qr Code   
            var margin = 0;
            var qrCodeWriter = new ZXing.BarcodeWriterPixelData
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    Height = height,
                    Width = width,
                    Margin = margin
                }
            };
            var pixelData = qrCodeWriter.Write(QrcodeContent);
            // creating a bitmap from the raw pixel data; if only black and white colors are used it makes no difference   
            // that the pixel data ist BGRA oriented and the bitmap is initialized with RGB   
            byte[] byteImage;
            using (var bitmap = new System.Drawing.Bitmap(pixelData.Width, pixelData.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb))
            using (var ms = new MemoryStream())
            {
                var bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, pixelData.Width, pixelData.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
                try
                {
                    // we assume that the row stride of the bitmap is aligned to 4 byte multiplied by the width of the image   
                    System.Runtime.InteropServices.Marshal.Copy(pixelData.Pixels, 0, bitmapData.Scan0, pixelData.Pixels.Length);
                }
                finally
                {
                    bitmap.UnlockBits(bitmapData);
                }
                // save to stream as PNG   
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                System.Drawing.Image img = System.Drawing.Image.FromStream(ms);

                byteImage = ms.ToArray();

                try
                {
                    string accesskey = "AKIAJO5FXGQ2M2N54QAQ";
                    string accesssecret = "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";
                    string bucket = "nursing-diary-store-s3/QRCode";
                    // connecting to the client
                    var client = new AmazonS3Client(accesskey, accesssecret, Amazon.RegionEndpoint.APSoutheast1);

                    // get the file and convert it to the byte[]
                    byte[] fileBytes = new Byte[qrcode.Length];                                     
                    // create unique file name for prevent the mess
                    var fileName = Guid.NewGuid() + qrcode;
                    PutObjectResponse response = null;

                    using (var stream = new MemoryStream(byteImage))
                    {
                        var request = new PutObjectRequest
                        {
                            BucketName = bucket,
                            Key = fileName,
                            InputStream = stream,
                            ContentType = "image/jpeg",
                            CannedACL = S3CannedACL.PublicRead
                        };

                        response = await client.PutObjectAsync(request);
                    };

                    if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    {
                        //this model is up to you, in my case I have to use it following;
                        return new UploadPhotoDto
                        {
                            Success = true,
                            FileName = fileName
                        };
                    }
                    else
                    {
                        //this model is up to you, in my case I have to use it following;
                        return new UploadPhotoDto
                        {
                            Success = false,
                            FileName = fileName
                        };
                    }
                }
                catch (AmazonS3Exception s3Exception)
                {
                    throw s3Exception;
                }
            }     
        }
    }
}
