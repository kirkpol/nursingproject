﻿using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Comment;
using NursingDiary.Logic.Models.Status;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Globalization;

namespace NursingDiary.Logic.Implement
{
    public class CommentService : ICommentService
    {
        private IEntityUnitOfWork EntityUnitOfWork { get; set; }

        public CommentService(IEntityUnitOfWork EntityUnitOfWork)
        {
            this.EntityUnitOfWork = EntityUnitOfWork;
        }

        public async Task<CommentDto> CreateCommentAsync(CommentDto model)
        {
            try
            {
                var check = await EntityUnitOfWork.DiaryRepository.GetSingleAsync(w => w.TeacherIdEachGroup == model.TeacherId && w.DiaryId == model.DiaryId);
                if (check != null)
                {
                    var data = new Comment()
                    {
                        CommentContent = model.CommentContent,
                        TeacherId = EntityUnitOfWork.DiaryRepository.GetSingleAsync(w => w.TeacherIdEachGroup == model.TeacherId).Result.TeacherIdEachGroup,
                        DiaryId = model.DiaryId,
                        DateComment = DateTime.Now
                    };
                    await EntityUnitOfWork.CommentRepository.AddAsync(data);
                    await EntityUnitOfWork.SaveAsync();

                    var teacherData = await EntityUnitOfWork.TeacherRepository.GetSingleAsync(g => g.TeacherId == data.TeacherId);

                    var commentData = new CommentDto()
                    {
                        CommentId = data.CommentId,
                        CommentContent = data.CommentContent,
                        DateComment = data.DateComment,
                        DiaryId = data.DiaryId,
                        TeacherId = data.TeacherId,
                        TeacherName = teacherData.Firstname + " " + teacherData.Lastname

                    };

                    return commentData;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception e)
            {
                return new CommentDto();
            }
        }

        public async Task<List<CommentDetailDto>> CommentDetailAsync(int diaryId)
        {
            try
            {
                CultureInfo _cultureTHInfo = new CultureInfo("th-TH");
                var data = await EntityUnitOfWork.CommentRepository.GetAll().Where(w => w.DiaryId == diaryId).ToListAsync();
                var result = data.Select(s => new CommentDetailDto() {
                    DiaryId = s.DiaryId,
                    CommentId = s.CommentId,
                    CommentContent = s.CommentContent,
                    DateComment = s.DateComment.ToString("dd MMM yyyy HH.mm น.", _cultureTHInfo),
                    TeacherId = s.TeacherId,
                    TeacherName = EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Firstname + " " + EntityUnitOfWork.TeacherRepository.GetSingleAsync(w => w.TeacherId == s.TeacherId).Result.Lastname               
                }).ToList();

                return result;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
