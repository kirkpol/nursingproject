﻿using Amazon.S3;
using Amazon.S3.Model;
using ImageWriter.Helper;
using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Upload;
using NursingDiary.Logic.Models.User;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Implement
{
    public class UploadService : IUploadService
    {
        private IEntityUnitOfWork EntityUnitOfWork { get; set; }

        public UploadService()
        {
            //this.EntityUnitOfWork = entityUnitOfWork;
        }

        public async Task<UploadPhotoDto> UploadObjectAsync(IFormFile image)
        {
            try
            {
                string accesskey = "AKIAJO5FXGQ2M2N54QAQ";
                string accesssecret = "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";
                string bucket = "nursing-diary-store-s3/DiaryImage";
                // connecting to the client
                var client = new AmazonS3Client(accesskey, accesssecret, Amazon.RegionEndpoint.APSoutheast1);

                // get the file and convert it to the byte[]
                byte[] fileBytes = new Byte[image.Length];
                image.OpenReadStream().Read(fileBytes, 0, Int32.Parse(image.Length.ToString()));

                // create unique file name for prevent the mess
                var fileName = Guid.NewGuid() + image.FileName;

                PutObjectResponse response = null;

                using (var stream = new MemoryStream(fileBytes))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = bucket,
                        Key = fileName,
                        InputStream = stream,
                        ContentType = image.ContentType,
                        CannedACL = S3CannedACL.PublicRead
                    };

                    response = await client.PutObjectAsync(request);
                };

                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    // this model is up to you, in my case I have to use it following;
                    return new UploadPhotoDto
                    {
                        Success = true,
                        FileName = fileName
                    };
                }
                else
                {
                    // this model is up to you, in my case I have to use it following;
                    return new UploadPhotoDto
                    {
                        Success = false,
                        FileName = fileName
                    };
                }
            }
            catch (AmazonS3Exception s3Exception)
            {
                throw s3Exception;
            }
        }

        public async Task<bool> ImportExcelFileAsync(IFormFile excel, int userType)
        {
            try
            {
                Teacher teacher = new Teacher();
                Student student = new Student();
                List<Teacher> teacherList = new List<Teacher>();
                List<Student> studentList = new List<Student>();

                var trippleDESAlgorithmService = new TrippleDESAlgorithmService();
                string newPath = Path.Combine(excel.FileName);
                if (excel.Length > 0)
                {
                    string sFileExtension = Path.GetExtension(excel.FileName).ToLower();
                    ISheet sheet;
                    string fullPath = Path.Combine(newPath, excel.FileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        excel.CopyTo(stream);
                        stream.Position = 0;
                        if (sFileExtension == ".xls")
                        {
                            HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                            sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                        }
                        else
                        {
                            XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                            sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                        }
                        IRow headerRow = sheet.GetRow(0); //Get Header Row
                        int cellCount = headerRow.LastCellNum;

                        switch (userType)
                        {
                            case 202: // อาจารย์
                                for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                                {
                                    IRow row = sheet.GetRow(i);
                                    if (row == null) continue;
                                    if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                                    for (int j = row.FirstCellNum; j < cellCount; j++)
                                    {
                                        switch (j)
                                        {
                                            case 0:
                                                teacher.TeacherId = row.GetCell(j).ToString();
                                                teacher.Password = await trippleDESAlgorithmService.EncryptAsync(row.GetCell(j).ToString());
                                                teacher.Type = 202;
                                                break;
                                            case 1:
                                                teacher.Firstname = row.GetCell(j).ToString();
                                                break;
                                            case 2:
                                                teacher.Lastname = row.GetCell(j).ToString();
                                                break;
                                        }
                                    }
                                    teacherList.Add(teacher);                                                                     
                                }
                                await EntityUnitOfWork.TeacherRepository.AddAsync(teacherList);
                                await EntityUnitOfWork.SaveAsync();
                                break;
                               

                            case 303: // นักเรียน
                                for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                                {
                                    IRow row = sheet.GetRow(i);
                                    if (row == null) continue;
                                    if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                                    for (int j = row.FirstCellNum; j < cellCount; j++)
                                    {
                                        switch (j)
                                        {
                                            case 0:
                                                student.StudentId = row.GetCell(j).ToString();
                                                student.Password = await trippleDESAlgorithmService.EncryptAsync(row.GetCell(j).ToString());
                                                student.Type = 303;
                                                break;
                                            case 1:
                                                student.Firstname = row.GetCell(j).ToString();
                                                break;
                                            case 2:
                                                student.Lastname = row.GetCell(j).ToString();
                                                break;
                                        }
                                    }
                                    studentList.Add(student);
                                }
                                await EntityUnitOfWork.StudentRepository.AddAsync(studentList);
                                await EntityUnitOfWork.SaveAsync();
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }

}

