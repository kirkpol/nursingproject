﻿using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Teacher;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Implement
{
    public class TeacherService : ITeacherService
    {
        private IEntityUnitOfWork EntityUnitOfWork { get; set; }

        public TeacherService(IEntityUnitOfWork entityUnitOfWork)
        {
            this.EntityUnitOfWork = entityUnitOfWork;
        }

        public async Task<bool> InsertTeacherAsync(TeacherDto model)
        {
            try
            {
                var trippleDESAlgorithmService = new TrippleDESAlgorithmService();
                var encrypt = trippleDESAlgorithmService.EncryptAsync(model.Password);
                Teacher teacher = new Teacher()
                {
                    TeacherId = model.TeacherId,
                    Password = await encrypt,
                    Firstname = model.Firstname,
                    Lastname = model.Lastname,
                    Type = 202
                };
                await EntityUnitOfWork.TeacherRepository.AddAsync(teacher);
                await EntityUnitOfWork.SaveAsync();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<TeacherDto> GetTeacherDetailAsync(string id)
        {
            try
            {
                var data = await EntityUnitOfWork.TeacherRepository.GetSingleAsync(g => g.TeacherId == id);
                var type = await EntityUnitOfWork.UserTypeRepository.GetSingleAsync(g => g.TypeId == data.Type);            
                TeacherDto result = new TeacherDto();

                result.TeacherId = data.TeacherId;
                result.Firstname = data.Firstname;
                result.Lastname = data.Lastname;
                result.TypeId = type.TypeId;
                
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<List<TeacherDto>> GetAllTeacherAsync()
        {
            try
            {
                var data = await EntityUnitOfWork.TeacherRepository.GetAll().ToListAsync();
                var result = data.Select(s => new TeacherDto()
                {                 
                    TeacherId = s.TeacherId,
                    Password = s.Password,
                    Firstname = s.Firstname,
                    Lastname = s.Lastname,
                    TypeId = 202
                }).ToList();
                return result;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<bool> DeleteTeacherAsync(string id)
        {
            try
            {
                var teacherData = await EntityUnitOfWork.TeacherRepository.GetSingleAsync(g => g.TeacherId == id);
                EntityUnitOfWork.TeacherRepository.Delete(teacherData);
                await EntityUnitOfWork.SaveAsync();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<bool> EditTeacherAsync(TeacherDto model)
        {
            try
            {
                var teacherData = await EntityUnitOfWork.TeacherRepository.GetSingleAsync(g => g.TeacherId == model.TeacherId);
                teacherData.Firstname = model.Firstname;
                teacherData.Lastname = model.Lastname;
                await EntityUnitOfWork.SaveAsync();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
