﻿using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Status;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Implement
{
    public class StatusService : IStatusService
    {
        private IEntityUnitOfWork EntityUnitOfWork { get; set; }

        public StatusService(IEntityUnitOfWork EntityUnitOfWork)
        {
            this.EntityUnitOfWork = EntityUnitOfWork; 
        }

        public async Task<bool> CreateStatusAsync(StatusDto model)
        {
            try
            {
                var status = new Status()
                {            
                    StatusType = model.StatusType,
                    StatusName = model.StatusName
                };

                await EntityUnitOfWork.StatusRepository.AddAsync(status);
                await EntityUnitOfWork.SaveAsync();
                
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}
