﻿using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Implement
{
    public class UserService : IUserService
    {
        private IEntityUnitOfWork EntityUnitOfWork { get; set; }

        public UserService(IEntityUnitOfWork entityUnitOfWork)
        {
            this.EntityUnitOfWork = entityUnitOfWork;
        }

        public async Task<UserDto> LoginAsync(string Username, string password)
        {
            try
            {
                var trippleDESAlgorithmService = new TrippleDESAlgorithmService();
                var encrypt = trippleDESAlgorithmService.EncryptAsync(password);

                var adminData = await EntityUnitOfWork.AdminRepository.GetSingleAsync(g => g.Username.Equals(Username) && g.Password.Equals(encrypt.Result));
                var teacherData = await EntityUnitOfWork.TeacherRepository.GetSingleAsync(g => g.TeacherId.Equals(Username) && g.Password.Equals(encrypt.Result));
                var studentData = await EntityUnitOfWork.StudentRepository.GetSingleAsync(g => g.StudentId.Equals(Username) && g.Password.Equals(encrypt.Result));

                var userData = new UserDto();

                if (adminData != null)
                {
                    userData.UserId = adminData.Username;
                    userData.TypeId = 101;
                }
                else if (teacherData != null)
                {
                    userData.UserId = teacherData.TeacherId;
                    userData.Firstname = teacherData.Firstname;
                    userData.Lastname = teacherData.Lastname;
                    userData.TypeId = 202;
                }
                else if (studentData != null)
                {
                    userData.UserId = studentData.StudentId;
                    userData.Firstname = studentData.Firstname;
                    userData.Lastname = studentData.Lastname;
                    userData.TypeId = 303;
                }
                else
                {
                    return null;
                }

                return userData;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
