﻿using Microsoft.EntityFrameworkCore;
using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Status;
using NursingDiary.Logic.Models.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Implement
{
    public class StudentService : IStudentService
    {
        private IEntityUnitOfWork EntityUnitOfWork { get; set; }

        public StudentService(IEntityUnitOfWork EntityUnitOfWork)
        {
            this.EntityUnitOfWork = EntityUnitOfWork;
        }

        public async Task<bool> InsertStudentAsync(StudentDto model)
        {
            try
            {
                var trippleDESAlgorithmService = new TrippleDESAlgorithmService();
                var encrypt = trippleDESAlgorithmService.EncryptAsync(model.Password);
                Student student = new Student()
                {
                    StudentId = model.StudentId,
                    Password = await encrypt,
                    Firstname = model.Firstname,
                    Lastname = model.Lastname,
                    Nickname = model.Nickname,
                    Type = 303
                };
                await EntityUnitOfWork.StudentRepository.AddAsync(student);
                await EntityUnitOfWork.SaveAsync();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<StudentDto> GetStudentDetailAsync(string id)
        {
            try
            {
                var data = await EntityUnitOfWork.StudentRepository.GetSingleAsync(g => g.StudentId == id);
                var type = await EntityUnitOfWork.UserTypeRepository.GetSingleAsync(g => g.TypeId == data.Type);
                StudentDto result = new StudentDto();

                result.StudentId = data.StudentId;
                result.Firstname = data.Firstname;
                result.Lastname = data.Lastname;
                result.TypeId = type.TypeId;

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<List<StudentDto>> GetAllStudentAsync()
        {
            try
            {
                var data = await EntityUnitOfWork.StudentRepository.GetAll().ToListAsync();
                var result = data.Select(s => new StudentDto()
                {
                    StudentId = s.StudentId,
                    Password = s.Password,
                    Firstname = s.Firstname,
                    Lastname = s.Lastname,
                    Nickname = s.Nickname,
                    TypeId = 303
                }).ToList();
                return result;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<bool> DeleteStudentAsync(string id)
        {
            try
            {
                var studentData = await EntityUnitOfWork.StudentRepository.GetSingleAsync(g => g.StudentId == id);
                var diaryData = await EntityUnitOfWork.DiaryRepository.GetAll(g => g.StudentId == studentData.StudentId).ToListAsync();
                var groupData = await EntityUnitOfWork.GroupRepository.GetSingleAsync(g => g.StudentId == studentData.StudentId);

                for (int i = 0; i< diaryData.Count; i++)
                {
                    var pictureData = await EntityUnitOfWork.DiaryPictureRepository.GetAll(g => g.DiaryId == diaryData[i].DiaryId).ToListAsync();
                    var hashTagChild = await EntityUnitOfWork.HashtagDiaryRepository.GetAll(g => g.DiaryId == diaryData[i].DiaryId).ToListAsync();

                    EntityUnitOfWork.HashtagDiaryRepository.Delete(hashTagChild);                  
                    EntityUnitOfWork.DiaryPictureRepository.Delete(pictureData);
                }

                if(diaryData.Count > 0)
                {
                    EntityUnitOfWork.DiaryRepository.Delete(diaryData);
                }

                if(groupData != null)
                {
                    EntityUnitOfWork.GroupRepository.Delete(groupData);
                }          
                EntityUnitOfWork.StudentRepository.Delete(studentData);
                await EntityUnitOfWork.SaveAsync();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<bool> EditStudentAsync(StudentDto model)
        {
            try {
                var studentData = await EntityUnitOfWork.StudentRepository.GetSingleAsync(g => g.StudentId == model.StudentId);
                studentData.Firstname = model.Firstname;
                studentData.Lastname = model.Lastname;
                await EntityUnitOfWork.SaveAsync();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }

}
