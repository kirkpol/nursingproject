﻿using Microsoft.EntityFrameworkCore;
using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Diary;
using NursingDiary.Logic.Models.Status;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.Logic.Implement
{
    public class DiaryService : IDiaryService
    {
        private IEntityUnitOfWork EntityUnitOfWork { get; set; }

        public DiaryService(IEntityUnitOfWork EntityUnitOfWork)
        {
            this.EntityUnitOfWork = EntityUnitOfWork;
        }

        public async Task<bool> RecordDiaryAsync(DiaryCreateDto model)
        {
            try
            {   // Save diary
                var diaryData = new Diary()
                {
                    DiaryTitle = model.DiaryTitle,
                    DiaryContent = model.DiaryContent,
                    DateCreate = DateTime.Now,
                    //DateSend = DateTime.Now,
                   // DateUpdate = DateTime.Now,
                    DateWard = model.DateWard,
                    StudentId = model.StudentId,
                    SubjectId = model.SubjectId,
                    StatusId = 200,
                    TeacherIdEachGroup = EntityUnitOfWork.GroupRepository.GetSingleAsync(w => w.ClassId == model.SubjectId && w.StudentId == model.StudentId).Result.TeacherId
                };              
                await EntityUnitOfWork.DiaryRepository.AddAsync(diaryData);

                // Save Picture
                if (model.PictureList != null)
                {
                    var DiaryPicture = new DiaryPicture()
                    {
                        DiaryId = diaryData.DiaryId,
                        PictureName = "https://s3-ap-southeast-1.amazonaws.com/nursing-diary-store-s3/DiaryImage/" + model.PictureList.PicName
                    };
                    //var diaryPicData = model.PictureList.Select(item => new DiaryPicture()
                    //{
                    //    DiaryId = diaryData.DiaryId,
                    //    PictureName = "https://s3-ap-southeast-1.amazonaws.com/nursing-diary-store-s3/DiaryImage/" + item.PicName
                    //}).ToList();
                    await EntityUnitOfWork.DiaryPictureRepository.AddAsync(DiaryPicture);
                }

                if (model.HashTagList != null)
                {
                    var hashTagData = model.HashTagList.Select(item => new Hashtag()
                    {                       
                        HashTagContent = item.HashTagName,
                    }).ToList();
                    await EntityUnitOfWork.HashtagRepository.AddAsync(hashTagData);
                    await EntityUnitOfWork.SaveAsync();
                    var hashTagChild = hashTagData.Select(item => new HashtagDiary()
                    {
                        DiaryId = diaryData.DiaryId,
                        HashtagId = item.HashTagId
                    }).ToList();
                    await EntityUnitOfWork.HashtagDiaryRepository.AddAsync(hashTagChild);
                    await EntityUnitOfWork.SaveAsync();
                }

                await EntityUnitOfWork.SaveAsync();

                
                /*var diary = new DiaryDetailDto()
                {
                    DiaryId = diaryData.DiaryId,
                    DiaryTitle = diaryData.DiaryTitle,
                    PictureCount = model.PictureList.Count
                };*/
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<bool> SuccessDiaryAsyncs(int diaryId)
        {
            try
            {
                var diary = await EntityUnitOfWork.DiaryRepository.GetSingleAsync(w => w.DiaryId == diaryId);             
                diary.StatusId = 100;
                diary.DateSend = DateTime.Now; 
                await EntityUnitOfWork.SaveAsync();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public async Task<bool> DiaryEditeAsync(DiaryDetailDto model)
        {
            try
            {
                var diaryBase = await EntityUnitOfWork.DiaryRepository.GetSingleAsync(w => w.DiaryId == model.DiaryId);

                diaryBase.DiaryTitle = model.DiaryTitle;
                diaryBase.DateWard = model.DateWard;

                return true;
            }catch(Exception e)
            {
                return false;
            }
        }

        public async Task<DiaryDetailDto> DiaryDetailAsync(int diaryId)
        {
            CultureInfo _cultureTHInfo = new CultureInfo("th-TH");
            try
            {
                var data = await EntityUnitOfWork.DiaryRepository.GetSingleAsync(w => w.DiaryId == diaryId);
                var diary = new DiaryDetailDto()
                {


                    DiaryId = data.DiaryId,
                    DiaryTitle = data.DiaryTitle,
                    DiaryContent = data.DiaryContent,
                    DateCreateToString = data.DateCreate.ToString("dd MMM yyyy", _cultureTHInfo),
                    DateWardToString = data.DateWard.ToString("dd MMM yyyy", _cultureTHInfo),
                    DateWard = data.DateWard,
                    DateCreate = data.DateCreate,
                    StatusId = data.StatusId,
                    StudentId = data.StudentId,
                    SubjectId = (int)data.SubjectId,
                    SubjectName = EntityUnitOfWork.SubjectRepository.GetSingleAsync( w => w.ClassId == data.SubjectId).Result.ClassName,
                    TeacherId = data.TeacherIdEachGroup
                };

                var picData = await EntityUnitOfWork.DiaryPictureRepository.GetAll().Where(w => w.DiaryId == diaryId).ToListAsync();
                if (picData != null)
                {
                    diary.diaryPictureDetials = picData.Select(s => new PictureDto()
                    {
                        PicId = s.PictureId,
                        PicName = s.PictureName
                    }).ToList();
                }

                var hashtagListData = await EntityUnitOfWork.HashtagDiaryRepository.GetAll().Where(w => w.DiaryId == diaryId).ToListAsync();             
                List<HasgtagDto> hasgtagList = new List<HasgtagDto>();
                if (hashtagListData != null)
                {
                    for(int i = 0; i< hashtagListData.Count; i++)
                    {
                        var hashtag = new HasgtagDto();
                        var hashtagData = await EntityUnitOfWork.HashtagRepository.GetSingleAsync(w => w.HashTagId == hashtagListData[i].HashtagId);
                        hashtag.HashTagId = hashtagData.HashTagId;
                        hashtag.HashTagName = hashtagData.HashTagContent;
                        hasgtagList.Add(hashtag);                       
                    }
                }
                diary.diaryHashtagDetails = hasgtagList;
                return diary;
            }        
            catch(Exception e)
            {
                return null;
            }
        }

        public async Task<List<DiaryListDto>> GetAllDiaryListAsync(string studentId)
        {
            try
            {
                CultureInfo _cultureTHInfo = new CultureInfo("th-TH");
                var data = await EntityUnitOfWork.DiaryRepository.GetAll(w =>w.StudentId == studentId).ToListAsync();
                var result = data.Select(s => new DiaryListDto()
                {

                    DiaryId = s.DiaryId,
                    StatusId = (int)s.StatusId,
                    SubjectId = (int)s.SubjectId,
                    subjectname = EntityUnitOfWork.SubjectRepository.GetSingleAsync( w => w.ClassId == s.SubjectId).Result.ClassName,
                    DiaryTitle = s.DiaryTitle,
                    StudentId = s.StudentId,
                    StudentName = EntityUnitOfWork.StudentRepository.GetSingleAsync(w => w.StudentId == s.StudentId).Result.Firstname + " " + EntityUnitOfWork.StudentRepository.GetSingleAsync(w => w.StudentId == s.StudentId).Result.Lastname,
                    DateWard = s.DateWard.ToString("dd MMM yyyy", _cultureTHInfo)
                }).ToList();

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<List<DiaryListDto>>DiaryTeacherListAsync(DiaryParaDto model)
        {
            try
            {
                CultureInfo _cultureTHInfo = new CultureInfo("th-TH");
                var data = await EntityUnitOfWork.DiaryRepository.GetAll(w => w.TeacherIdEachGroup == model.TeacherId && w.SubjectId == model.SubjectId).OrderBy(w => w.DateWard).ToListAsync();
                var result = data.Select(s => new DiaryListDto() {

                    DiaryId = s.DiaryId,
                    StatusId = (int)s.StatusId,
                    SubjectId = (int)s.SubjectId,
                    DiaryTitle = s.DiaryTitle,
                    StudentId = s.StudentId,
                    StudentName = EntityUnitOfWork.StudentRepository.GetSingleAsync( w => w.StudentId == s.StudentId).Result.Firstname +" " + EntityUnitOfWork.StudentRepository.GetSingleAsync(w => w.StudentId == s.StudentId).Result.Lastname,
                    DateWard = s.DateWard.ToString("dd MMM yyyy", _cultureTHInfo)               
                }).ToList();

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<List<DiaryListDto>> DiaryStudentListAsync(DiaryParaDto model)
        {
            try
            {
                CultureInfo _cultureTHInfo = new CultureInfo("th-TH");
                var data = await EntityUnitOfWork.DiaryRepository.GetAll(w => w.StudentId == model.StudentId && w.SubjectId == model.SubjectId).OrderBy(w => w.DateWard).ToListAsync();
                var result = data.Select(s => new DiaryListDto()
                {

                    DiaryId = s.DiaryId,
                    StatusId = (int)s.StatusId,
                    SubjectId = (int)s.SubjectId,
                    DiaryTitle = s.DiaryTitle,
                    StudentId = s.StudentId,
                    StudentName = EntityUnitOfWork.StudentRepository.GetSingleAsync(w => w.StudentId == s.StudentId).Result.Firstname + " " + EntityUnitOfWork.StudentRepository.GetSingleAsync(w => w.StudentId == s.StudentId).Result.Lastname,
                    DateWard = s.DateWard.ToString("dd MMM yyyy", _cultureTHInfo)
                }).ToList();

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}

