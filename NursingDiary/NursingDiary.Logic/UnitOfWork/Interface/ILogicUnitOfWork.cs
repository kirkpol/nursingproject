﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Interface
{
    public interface ILogicUnitOfWork
    {
        ITrippleDESAlgorithmService TrippleDESAlgorithmService { get; set; }
        IStatusService StatusService { get; set; }
        ITeacherService TeacherService { get; set; }
        IStudentService StudentService { get; set; }
        IDiaryService DiaryService { get; set; }
        ISubjectService SubjectService { get; set; }
        ICommentService CommentService { get; set; }
        IUserService UserService { get; set; }
        IUploadService UploadService { get; set; }
    }
}
