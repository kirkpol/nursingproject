﻿using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.Logic.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Implement
{
    public class LogicUnitOfWork : ILogicUnitOfWork
    {
        private readonly IEntityUnitOfWork entityUnitOfWork;

        public LogicUnitOfWork(IEntityUnitOfWork entityUnitOfWork)
        {
            this.entityUnitOfWork = entityUnitOfWork;
        }

        private ITrippleDESAlgorithmService trippleDESAlgorithmService;
        private IStatusService statusService;
        private ITeacherService teacherService;
        private IStudentService studentService;
        private IDiaryService diaryService;
        private ISubjectService subjectService;
        private ICommentService commentService;
        private IUserService userService;
        private IUploadService uploadService;

        public ITrippleDESAlgorithmService TrippleDESAlgorithmService
        {
            get { return trippleDESAlgorithmService ?? (trippleDESAlgorithmService = new TrippleDESAlgorithmService()); }
            set { trippleDESAlgorithmService = value; }
        }

        public IStatusService StatusService
        {
            get { return statusService ?? (statusService = new StatusService(entityUnitOfWork)); }
            set { statusService = value; }
        }

        public ITeacherService TeacherService
        {
            get { return teacherService ?? (teacherService = new TeacherService(entityUnitOfWork)); }
            set { teacherService = value; }
        }

        public IStudentService StudentService
        {
            get { return studentService ?? (studentService = new StudentService(entityUnitOfWork)); }
            set { studentService = value; }
        }

        public IDiaryService DiaryService
        {
            get { return diaryService ?? (diaryService = new DiaryService(entityUnitOfWork)); }
            set { diaryService = value; }
        }

        public ISubjectService SubjectService
        {
            get { return subjectService ?? (subjectService = new SubjectService(entityUnitOfWork)); }
            set { subjectService = value; }
        }

        public ICommentService CommentService
        {
            get { return commentService ?? (commentService = new CommentService(entityUnitOfWork)); }
            set { commentService = value; }
        }

        public IUserService UserService
        {
            get { return userService ?? (userService = new UserService(entityUnitOfWork)); }
            set { userService = value; }
        }

        public IUploadService UploadService
        {
            get { return uploadService ?? (uploadService = new UploadService()); }
            set { uploadService = value; }
        }

    }
}
