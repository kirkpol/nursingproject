﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    [Table("Teacher_Helper")]
    public partial class TeacherHelper
    {
        public int Id { get; set; }
        [Column("Class_Id")]
        public int? ClassId { get; set; }
        [Column("Teacher_Id")]
        [StringLength(100)]
        public string TeacherId { get; set; }
        [StringLength(50)]
        public string TeacherType { get; set; }

        [ForeignKey("ClassId")]
        [InverseProperty("TeacherHelper")]
        public Subject Class { get; set; }
        [ForeignKey("TeacherId")]
        [InverseProperty("TeacherHelper")]
        public Teacher Teacher { get; set; }
    }
}
