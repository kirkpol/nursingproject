﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Group
    {
        public int Id { get; set; }
        [Column("Class_Id")]
        public int? ClassId { get; set; }
        [Column("Student_Id")]
        [StringLength(100)]
        public string StudentId { get; set; }
        [Column("Teacher_Id")]
        [StringLength(100)]
        public string TeacherId { get; set; }
        [StringLength(50)]
        public string TeacherType { get; set; }

        [ForeignKey("ClassId")]
        [InverseProperty("Group")]
        public Subject Class { get; set; }
        [ForeignKey("StudentId")]
        [InverseProperty("Group")]
        public Student Student { get; set; }
        [ForeignKey("TeacherId")]
        [InverseProperty("Group")]
        public Teacher Teacher { get; set; }
    }
}
