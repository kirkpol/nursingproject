﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Diary
    {
        public Diary()
        {
            Comment = new HashSet<Comment>();
            DiaryPictureNavigation = new HashSet<DiaryPicture>();
            HashtagDiary = new HashSet<HashtagDiary>();
        }

        [Column("Diary_Id")]
        public int DiaryId { get; set; }
        [Required]
        [Column("Diary_Title")]
        public string DiaryTitle { get; set; }
        [Column("Diary_Content")]
        public string DiaryContent { get; set; }
        [Column("Diary_Picture")]
        public string DiaryPicture { get; set; }
        [Column("Date_Create", TypeName = "datetime")]
        public DateTime DateCreate { get; set; }
        [Column("Date_Send", TypeName = "datetime")]
        public DateTime? DateSend { get; set; }
        [Column("Date_Ward", TypeName = "date")]
        public DateTime DateWard { get; set; }
        [Column("Date_Update", TypeName = "datetime")]
        public DateTime? DateUpdate { get; set; }
        [Column("Status_Id")]
        public int? StatusId { get; set; }
        [Column("Student_Id")]
        [StringLength(100)]
        public string StudentId { get; set; }
        [Column("Subject_id")]
        public int? SubjectId { get; set; }
        [StringLength(100)]
        public string TeacherIdEachGroup { get; set; }

        [ForeignKey("StatusId")]
        [InverseProperty("Diary")]
        public Status Status { get; set; }
        [ForeignKey("StudentId")]
        [InverseProperty("Diary")]
        public Student Student { get; set; }
        [ForeignKey("SubjectId")]
        [InverseProperty("Diary")]
        public Subject Subject { get; set; }
        [InverseProperty("Diary")]
        public ICollection<Comment> Comment { get; set; }
        [InverseProperty("Diary")]
        public ICollection<DiaryPicture> DiaryPictureNavigation { get; set; }
        [InverseProperty("Diary")]
        public ICollection<HashtagDiary> HashtagDiary { get; set; }
    }
}
