﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Comment
    {
        [Column("Comment_Id")]
        public int CommentId { get; set; }
        [Column("Comment_Content")]
        public string CommentContent { get; set; }
        [Column("Date_Comment", TypeName = "datetime")]
        public DateTime DateComment { get; set; }
        [Column("Diary_Id")]
        public int DiaryId { get; set; }
        [Required]
        [Column("Teacher_Id")]
        [StringLength(100)]
        public string TeacherId { get; set; }

        [ForeignKey("DiaryId")]
        [InverseProperty("Comment")]
        public Diary Diary { get; set; }
        [ForeignKey("TeacherId")]
        [InverseProperty("Comment")]
        public Teacher Teacher { get; set; }
    }
}
