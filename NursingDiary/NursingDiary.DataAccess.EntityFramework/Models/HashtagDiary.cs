﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    [Table("Hashtag_Diary")]
    public partial class HashtagDiary
    {
        public int Id { get; set; }
        [Column("Diary_Id")]
        public int? DiaryId { get; set; }
        [Column("Hashtag_Id")]
        public int? HashtagId { get; set; }

        [ForeignKey("DiaryId")]
        [InverseProperty("HashtagDiary")]
        public Diary Diary { get; set; }
        [ForeignKey("HashtagId")]
        [InverseProperty("HashtagDiary")]
        public Hashtag Hashtag { get; set; }
    }
}
