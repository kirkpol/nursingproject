﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class DiaryPicture
    {
        [Key]
        [Column("Picture_Id")]
        public int PictureId { get; set; }
        [Column("Diary_id")]
        public int? DiaryId { get; set; }
        [Column("Picture_Name")]
        public string PictureName { get; set; }

        [ForeignKey("DiaryId")]
        [InverseProperty("DiaryPictureNavigation")]
        public Diary Diary { get; set; }
    }
}
