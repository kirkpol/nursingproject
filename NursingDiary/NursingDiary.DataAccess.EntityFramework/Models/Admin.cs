﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Admin
    {
        [Key]
        [StringLength(18)]
        public string Username { get; set; }
        public string Password { get; set; }
        public int? Type { get; set; }

        [ForeignKey("Type")]
        [InverseProperty("Admin")]
        public UserType TypeNavigation { get; set; }
    }
}
