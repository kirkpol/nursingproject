﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Location
    {
        public Location()
        {
            ClassLocation = new HashSet<ClassLocation>();
        }

        [Column("Location_Id")]
        public int LocationId { get; set; }
        [Column("Location")]
        [StringLength(50)]
        public string Location1 { get; set; }

        [InverseProperty("Location")]
        public ICollection<ClassLocation> ClassLocation { get; set; }
    }
}
