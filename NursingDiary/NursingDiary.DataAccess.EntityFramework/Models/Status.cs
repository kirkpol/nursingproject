﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Status
    {
        public Status()
        {
            Diary = new HashSet<Diary>();
        }

        [Key]
        [Column("Status_Type")]
        public int StatusType { get; set; }
        [Column("Status_Name")]
        [StringLength(30)]
        public string StatusName { get; set; }

        [InverseProperty("Status")]
        public ICollection<Diary> Diary { get; set; }
    }
}
