﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Student
    {
        public Student()
        {
            Diary = new HashSet<Diary>();
            Group = new HashSet<Group>();
        }

        [Column("Student_Id")]
        [StringLength(100)]
        public string StudentId { get; set; }
        public string Password { get; set; }
        [StringLength(30)]
        public string Firstname { get; set; }
        [StringLength(30)]
        public string Lastname { get; set; }
        [StringLength(20)]
        public string Nickname { get; set; }
        public int? Type { get; set; }

        [ForeignKey("Type")]
        [InverseProperty("Student")]
        public UserType TypeNavigation { get; set; }
        [InverseProperty("Student")]
        public ICollection<Diary> Diary { get; set; }
        [InverseProperty("Student")]
        public ICollection<Group> Group { get; set; }
    }
}
