﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Hashtag
    {
        public Hashtag()
        {
            HashtagDiary = new HashSet<HashtagDiary>();
        }

        [Column("HashTag_Id")]
        public int HashTagId { get; set; }
        [Column("HashTag_Content")]
        public string HashTagContent { get; set; }

        [InverseProperty("Hashtag")]
        public ICollection<HashtagDiary> HashtagDiary { get; set; }
    }
}
