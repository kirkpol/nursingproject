﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Teacher
    {
        public Teacher()
        {
            Comment = new HashSet<Comment>();
            Group = new HashSet<Group>();
            Subject = new HashSet<Subject>();
            TeacherHelper = new HashSet<TeacherHelper>();
        }

        [Column("Teacher_Id")]
        [StringLength(100)]
        public string TeacherId { get; set; }
        public string Password { get; set; }
        [StringLength(50)]
        public string Firstname { get; set; }
        [StringLength(50)]
        public string Lastname { get; set; }
        public int? Type { get; set; }

        [ForeignKey("Type")]
        [InverseProperty("Teacher")]
        public UserType TypeNavigation { get; set; }
        [InverseProperty("Teacher")]
        public ICollection<Comment> Comment { get; set; }
        [InverseProperty("Teacher")]
        public ICollection<Group> Group { get; set; }
        [InverseProperty("Teacher")]
        public ICollection<Subject> Subject { get; set; }
        [InverseProperty("Teacher")]
        public ICollection<TeacherHelper> TeacherHelper { get; set; }
    }
}
