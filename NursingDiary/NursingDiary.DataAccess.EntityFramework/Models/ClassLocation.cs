﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    [Table("Class_Location")]
    public partial class ClassLocation
    {
        public int Id { get; set; }
        [Column("Class_Id")]
        public int? ClassId { get; set; }
        [Column("Location_Id")]
        public int? LocationId { get; set; }

        [ForeignKey("ClassId")]
        [InverseProperty("ClassLocation")]
        public Subject Class { get; set; }
        [ForeignKey("LocationId")]
        [InverseProperty("ClassLocation")]
        public Location Location { get; set; }
    }
}
