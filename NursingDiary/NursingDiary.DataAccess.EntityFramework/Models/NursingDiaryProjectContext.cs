﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class NursingDiaryProjectContext : DbContext
    {
        public NursingDiaryProjectContext()
        {
        }

        public NursingDiaryProjectContext(DbContextOptions<NursingDiaryProjectContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<ClassLocation> ClassLocation { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Diary> Diary { get; set; }
        public virtual DbSet<DiaryPicture> DiaryPicture { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<Hashtag> Hashtag { get; set; }
        public virtual DbSet<HashtagDiary> HashtagDiary { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<Subject> Subject { get; set; }
        public virtual DbSet<Teacher> Teacher { get; set; }
        public virtual DbSet<TeacherHelper> TeacherHelper { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=nursingdiarydatabase.cvjof5ahv0td.ap-southeast-1.rds.amazonaws.com;Database=NursingDiaryProject;Trusted_Connection=false;User Id=NursingDiarySys;Password=Nurs1ngD1ary;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>(entity =>
            {
                entity.Property(e => e.Username).ValueGeneratedNever();

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Admin)
                    .HasForeignKey(d => d.Type)
                    .HasConstraintName("FK_AdminUserType");
            });

            modelBuilder.Entity<ClassLocation>(entity =>
            {
                entity.HasOne(d => d.Class)
                    .WithMany(p => p.ClassLocation)
                    .HasForeignKey(d => d.ClassId)
                    .HasConstraintName("FK_ClassLocation_Class");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.ClassLocation)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_ClassLocation_Location");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.HasOne(d => d.Diary)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.DiaryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comment_Diary");

                entity.HasOne(d => d.Teacher)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.TeacherId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comment_Teacher");
            });

            modelBuilder.Entity<Diary>(entity =>
            {
                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Diary)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Diary_Status");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Diary)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_Diary_Student");

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.Diary)
                    .HasForeignKey(d => d.SubjectId)
                    .HasConstraintName("FK_Diary_Subject");
            });

            modelBuilder.Entity<DiaryPicture>(entity =>
            {
                entity.HasOne(d => d.Diary)
                    .WithMany(p => p.DiaryPictureNavigation)
                    .HasForeignKey(d => d.DiaryId)
                    .HasConstraintName("FK_DiaryPicture_Diary");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.HasOne(d => d.Class)
                    .WithMany(p => p.Group)
                    .HasForeignKey(d => d.ClassId)
                    .HasConstraintName("FK_Group_Class");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Group)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_Group_Student");

                entity.HasOne(d => d.Teacher)
                    .WithMany(p => p.Group)
                    .HasForeignKey(d => d.TeacherId)
                    .HasConstraintName("FK_Group_Teacher");
            });

            modelBuilder.Entity<HashtagDiary>(entity =>
            {
                entity.HasOne(d => d.Diary)
                    .WithMany(p => p.HashtagDiary)
                    .HasForeignKey(d => d.DiaryId)
                    .HasConstraintName("FK_HashtagDiary_Diary");

                entity.HasOne(d => d.Hashtag)
                    .WithMany(p => p.HashtagDiary)
                    .HasForeignKey(d => d.HashtagId)
                    .HasConstraintName("FK_Hashtag_Diary_Hashtag");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.Property(e => e.StatusType).ValueGeneratedNever();
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.Property(e => e.StudentId).ValueGeneratedNever();

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Student)
                    .HasForeignKey(d => d.Type)
                    .HasConstraintName("FK_StudentUserType");
            });

            modelBuilder.Entity<Subject>(entity =>
            {
                entity.HasOne(d => d.Teacher)
                    .WithMany(p => p.Subject)
                    .HasForeignKey(d => d.TeacherId)
                    .HasConstraintName("FK_Teacher_Id");
            });

            modelBuilder.Entity<Teacher>(entity =>
            {
                entity.Property(e => e.TeacherId).ValueGeneratedNever();

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Teacher)
                    .HasForeignKey(d => d.Type)
                    .HasConstraintName("FK_TeacherUserType");
            });

            modelBuilder.Entity<TeacherHelper>(entity =>
            {
                entity.HasOne(d => d.Class)
                    .WithMany(p => p.TeacherHelper)
                    .HasForeignKey(d => d.ClassId)
                    .HasConstraintName("FK_TeacherHelper_Class");

                entity.HasOne(d => d.Teacher)
                    .WithMany(p => p.TeacherHelper)
                    .HasForeignKey(d => d.TeacherId)
                    .HasConstraintName("FK_TeacherHelper_Teacher");
            });

            modelBuilder.Entity<UserType>(entity =>
            {
                entity.Property(e => e.TypeId).ValueGeneratedNever();
            });
        }
    }
}
