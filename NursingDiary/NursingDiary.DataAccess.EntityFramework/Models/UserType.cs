﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class UserType
    {
        public UserType()
        {
            Admin = new HashSet<Admin>();
            Student = new HashSet<Student>();
            Teacher = new HashSet<Teacher>();
        }

        [Key]
        public int TypeId { get; set; }
        [StringLength(20)]
        public string TypeName { get; set; }

        [InverseProperty("TypeNavigation")]
        public ICollection<Admin> Admin { get; set; }
        [InverseProperty("TypeNavigation")]
        public ICollection<Student> Student { get; set; }
        [InverseProperty("TypeNavigation")]
        public ICollection<Teacher> Teacher { get; set; }
    }
}
