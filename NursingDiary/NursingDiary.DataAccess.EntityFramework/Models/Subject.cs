﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NursingDiary.DataAccess.EntityFramework.Models
{
    public partial class Subject
    {
        public Subject()
        {
            ClassLocation = new HashSet<ClassLocation>();
            Diary = new HashSet<Diary>();
            Group = new HashSet<Group>();
            TeacherHelper = new HashSet<TeacherHelper>();
        }

        [Key]
        [Column("Class_Id")]
        public int ClassId { get; set; }
        [Required]
        [Column("Class_Name")]
        [StringLength(50)]
        public string ClassName { get; set; }
        [Column("Teacher_Id")]
        [StringLength(100)]
        public string TeacherId { get; set; }
        [Required]
        [Column("QRCode")]
        [StringLength(10)]
        public string Qrcode { get; set; }
        [Column("QRCodePath")]
        public string QrcodePath { get; set; }

        [ForeignKey("TeacherId")]
        [InverseProperty("Subject")]
        public Teacher Teacher { get; set; }
        [InverseProperty("Class")]
        public ICollection<ClassLocation> ClassLocation { get; set; }
        [InverseProperty("Subject")]
        public ICollection<Diary> Diary { get; set; }
        [InverseProperty("Class")]
        public ICollection<Group> Group { get; set; }
        [InverseProperty("Class")]
        public ICollection<TeacherHelper> TeacherHelper { get; set; }
    }
}
