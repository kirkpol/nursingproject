﻿using Microsoft.EntityFrameworkCore;
using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.DataAccess.EntityFramework.Implement
{
    public class EntityUnitOfWork : IEntityUnitOfWork
    {
        private readonly DbContext context;

        private IEntityFrameworkRepository<Admin> IAdminRepository;
        private IEntityFrameworkRepository<Subject> ISubjectRepository;
        private IEntityFrameworkRepository<ClassLocation> IClassLocationRepository;
        private IEntityFrameworkRepository<Comment> ICommentRepository;
        private IEntityFrameworkRepository<Diary> IDiaryRepository;
        private IEntityFrameworkRepository<DiaryPicture> IDiaryPictureRepository;
        private IEntityFrameworkRepository<Group> IGroupRepository;
        private IEntityFrameworkRepository<Hashtag> IHashtagRepository;
        private IEntityFrameworkRepository<HashtagDiary> IHashtagDiaryRepository;
        private IEntityFrameworkRepository<Location> ILocationRepository;
        private IEntityFrameworkRepository<Status> IStatusRepository;
        private IEntityFrameworkRepository<Student> IStudentRepository;
        private IEntityFrameworkRepository<Teacher> ITeacherRepository;
        private IEntityFrameworkRepository<TeacherHelper> ITeacherHelperRepository;
        private IEntityFrameworkRepository<UserType> IUserTypeRepository;

        public EntityUnitOfWork(IEntityFrameworkContext context)
        {
            this.context = context.GetConnection();
        }

        public IEntityFrameworkRepository<Admin> AdminRepository
        {
            get { return IAdminRepository ?? (IAdminRepository = new EntityFrameworkRepository<Admin>(context)); }
            set { IAdminRepository = value; }
        }

        public IEntityFrameworkRepository<Subject> SubjectRepository
        {
            get { return ISubjectRepository ?? (ISubjectRepository = new EntityFrameworkRepository<Subject>(context)); }
            set { ISubjectRepository = value; }
        }

        public IEntityFrameworkRepository<ClassLocation> ClassLocationRepository
        {
            get { return IClassLocationRepository ?? (IClassLocationRepository = new EntityFrameworkRepository<ClassLocation>(context)); }
            set { IClassLocationRepository = value; }
        }

        public IEntityFrameworkRepository<Comment> CommentRepository
        {
            get { return ICommentRepository ?? (ICommentRepository = new EntityFrameworkRepository<Comment>(context)); }
            set { ICommentRepository = value; }
        }

        public IEntityFrameworkRepository<Diary> DiaryRepository
        {
            get { return IDiaryRepository ?? (IDiaryRepository = new EntityFrameworkRepository<Diary>(context)); }
            set { IDiaryRepository = value; }
        }

        public IEntityFrameworkRepository<DiaryPicture> DiaryPictureRepository
        {
            get { return IDiaryPictureRepository ?? (IDiaryPictureRepository = new EntityFrameworkRepository<DiaryPicture>(context)); }
            set { IDiaryPictureRepository = value; }
        }


        public IEntityFrameworkRepository<Group> GroupRepository
        {
            get { return IGroupRepository ?? (IGroupRepository = new EntityFrameworkRepository<Group>(context)); }
            set { IGroupRepository = value; }
        }

        public IEntityFrameworkRepository<Hashtag> HashtagRepository
        {
            get { return IHashtagRepository ?? (IHashtagRepository = new EntityFrameworkRepository<Hashtag>(context)); }
            set { IHashtagRepository = value; }
        }

        public IEntityFrameworkRepository<HashtagDiary> HashtagDiaryRepository
        {
            get { return IHashtagDiaryRepository ?? (IHashtagDiaryRepository = new EntityFrameworkRepository<HashtagDiary>(context)); }
            set { IHashtagDiaryRepository = value; }
        }

        public IEntityFrameworkRepository<Location> LocationRepository
        {
            get { return ILocationRepository ?? (ILocationRepository = new EntityFrameworkRepository<Location>(context)); }
            set { ILocationRepository = value; }
        }

        public IEntityFrameworkRepository<Status> StatusRepository
        {
            get { return IStatusRepository ?? (IStatusRepository = new EntityFrameworkRepository<Status>(context)); }
            set { IStatusRepository = value; }
        }

        public IEntityFrameworkRepository<Student> StudentRepository
        {
            get { return IStudentRepository ?? (IStudentRepository = new EntityFrameworkRepository<Student>(context)); }
            set { IStudentRepository = value; }
        }

        public IEntityFrameworkRepository<Teacher> TeacherRepository
        {
            get { return ITeacherRepository ?? (ITeacherRepository = new EntityFrameworkRepository<Teacher>(context)); }
            set { ITeacherRepository = value; }
        }

        public IEntityFrameworkRepository<TeacherHelper> TeacherHelperRepository
        {
            get { return ITeacherHelperRepository ?? (ITeacherHelperRepository = new EntityFrameworkRepository<TeacherHelper>(context)); }
            set { ITeacherHelperRepository = value; }
        }

        public IEntityFrameworkRepository<UserType> UserTypeRepository
        {
            get { return IUserTypeRepository ?? (IUserTypeRepository = new EntityFrameworkRepository<UserType>(context)); }
            set { IUserTypeRepository = value; }
        }

        public Task<int> SaveAsync()
        {
            return context.SaveChangesAsync();
        }
    }
}
