﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.DataAccess.EntityFramework.Models;
namespace NursingDiary.DataAccess.EntityFramework.Implement
{
    public class EntityFrameworkContext : IEntityFrameworkContext
    {
        public DbContext GetConnection() => new NursingDiaryProjectContext();
    }
}
