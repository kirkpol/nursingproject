﻿using NursingDiary.DataAccess.EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NursingDiary.DataAccess.EntityFramework.Interface
{
    public interface IEntityUnitOfWork
    {
        IEntityFrameworkRepository<Admin> AdminRepository { get; set; }
        IEntityFrameworkRepository<ClassLocation> ClassLocationRepository { get; set; }
        IEntityFrameworkRepository<Comment> CommentRepository { get; set; }
        IEntityFrameworkRepository<Diary> DiaryRepository { get; set; }
        IEntityFrameworkRepository<Group> GroupRepository { get; set; }
        IEntityFrameworkRepository<Hashtag> HashtagRepository { get; set; }
        IEntityFrameworkRepository<HashtagDiary> HashtagDiaryRepository { get; set; }
        IEntityFrameworkRepository<Location> LocationRepository { get; set; }
        IEntityFrameworkRepository<Status> StatusRepository { get; set; }
        IEntityFrameworkRepository<Student> StudentRepository { get; set; }
        IEntityFrameworkRepository<Subject> SubjectRepository { get; set; }
        IEntityFrameworkRepository<Teacher> TeacherRepository { get; set; }
        IEntityFrameworkRepository<TeacherHelper> TeacherHelperRepository { get; set; }
        IEntityFrameworkRepository<UserType> UserTypeRepository { get; set; }
        IEntityFrameworkRepository<DiaryPicture> DiaryPictureRepository { get; set; }
        Task<int> SaveAsync();
    }
}
