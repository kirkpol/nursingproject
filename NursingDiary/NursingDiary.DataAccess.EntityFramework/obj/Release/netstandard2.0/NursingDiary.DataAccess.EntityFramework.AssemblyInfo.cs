//------------------------------------------------------------------------------
// <auto-generated>
//     Generated by the MSBuild WriteCodeFragment class.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: System.Reflection.AssemblyCompanyAttribute("NursingDiary.DataAccess.EntityFramework")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Release")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("1.0.0.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("1.0.0")]
[assembly: System.Reflection.AssemblyProductAttribute("NursingDiary.DataAccess.EntityFramework")]
[assembly: System.Reflection.AssemblyTitleAttribute("NursingDiary.DataAccess.EntityFramework")]
[assembly: System.Reflection.AssemblyVersionAttribute("1.0.0.0")]
