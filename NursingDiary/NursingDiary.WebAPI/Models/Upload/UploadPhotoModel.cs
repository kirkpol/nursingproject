﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NursingDiary.Logic.Models.Upload
{
   public class UploadPhotoModel
    {
        public bool Success { get; set; } 
        public string FileName { get; set; }
    }
}
