﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Subject
{
    public class SubjectTeacherPara
    {
        public string TeacherId { get; set; }
    }
}
