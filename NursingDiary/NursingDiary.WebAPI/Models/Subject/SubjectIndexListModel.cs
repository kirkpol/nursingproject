﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Subject
{
    public class SubjectIndexListModel
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string StudentId { get; set; }
        public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherType { get; set; }
    }
}
