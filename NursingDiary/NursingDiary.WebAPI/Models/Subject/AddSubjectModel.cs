﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Subject
{
    public class AddSubjectModel
    {
        public string StudentId { get; set; }
        public string TeacherId { get; set; }
        public string Qrcode { get; set; }
    }
}
