﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Subject
{
    public class SubjectModel
    {
        public string SubjectName { get; set; }     
        public string TeacherId { get; set; }
        public List<TeacherAssistantModel>TeacherAssistants { get; set; }
    }

    public class TeacherAssistantModel
    {
        public string TeacherId { get; set; }
    }
}
