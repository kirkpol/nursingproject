﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Subject
{
    public class SubjectSingleDetailModel
    {
        public string SubjectName { get; set; }
        public string QRCode { get; set; }
        public string QRCodePath { get; set; }
        public string TeacherHeadId { get; set; }
        public string TeacherHeadName { get; set; }
        public string TeacherAssistantId { get; set; }
        public string TeacherAssistantName { get; set; }
    }
}
