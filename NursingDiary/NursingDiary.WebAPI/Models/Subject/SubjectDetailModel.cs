﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Subject
{
    public class SubjectDetailModel
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string QRCode { get; set; }
        public string QRCodePath{get; set;}
        public List<TeacherAssistantDetailModel> TeacherAssistants { get; set; }
    }

    public class TeacherAssistantDetailModel
    {
        public string TeacherName { get; set; }
        public string TeacherId { get; set; }
    }
}


