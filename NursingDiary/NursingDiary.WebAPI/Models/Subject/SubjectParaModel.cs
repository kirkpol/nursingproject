﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Subject
{
    public class SubjectParaModel
    {
        public int SubjectId { get; set; }
        public string QRCode { get; set; }
    }
}
