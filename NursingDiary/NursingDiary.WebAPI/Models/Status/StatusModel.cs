﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Status
{
    public class StatusModel
    {
        public int StatusType { get; set; }
        public string StatusName { get; set; }
    }
}
