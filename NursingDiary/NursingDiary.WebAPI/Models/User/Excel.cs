﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.User
{
    public class Excel
    {
       public IFormFile file { get; set; }
       public string UserType { get; set; }
    }
}
