﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Diary
{
    public class DiaryDetailModel
    {
        public int? DiaryId { get; set; }
        public string DiaryTitle { get; set; }
        public string DiaryContent { get; set; }
        public string DateSend { get; set; }
        public string DateWardToString { get; set; }
        public string DateCreateToString { get; set; }
        public DateTime DateWard { get; set; }
        public DateTime DateCreate { get; set; }
        public string DateUpdate { get; set; } // Auto เมื่อมาแก้ไข
        public int? StatusId { get; set; }
        public string StudentId { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string TeacherId { get; set; }
        public List<DiaryPictureModel> diaryPictureDetials { get; set; }
        public List<DiaryHashTagModel> diaryHashtagDetails { get; set; }


        public class DiaryPictureModel
        {
            public int PicId { get; set; }
            public string PicName { get; set; }
        }

        public class DiaryHashTagModel
        {
            public int HashTagId { get; set; }
            public string HashTagName { get; set; }
        }
    }
}
