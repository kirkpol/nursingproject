﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Diary
{
    public class DiaryCreateModel
    {
        public string DiaryTitle { get; set; }
        public string DiaryContent { get; set; }
        public DateTime DateCreate { get; set; }  // วันที่สร้างรับค่าจากผู้ใช้
        public DateTime DateWard { get; set; }    // เวลาขึ้น Ward
        public int? StatusId { get; set; }
        public string StudentId { get; set; }
        public int SubjectId { get; set; }
        public IFormFile UploadImage { get; set; }
        public DiaryPictureCreateModel PictureList { get; set; }
        public List<DiaryHashTagCreateModel> HashTagList { get; set; }
    }

    public class DiaryPictureCreateModel
    {
        public string PicName { get; set; }
    }

    public class DiaryHashTagCreateModel
    {
        public string HashTagName { get; set; }
    }
}

