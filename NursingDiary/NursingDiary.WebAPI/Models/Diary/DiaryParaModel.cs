﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Diary
{
    public class DiaryParaModel
    {
        public string StudentId { get; set; }
        public string TeacherId { get; set; }
        public int SubjectId { get; set; }
    }

    public class DiarySingleParaModel
    {
        public string StudentId { get; set; }
    }
}
