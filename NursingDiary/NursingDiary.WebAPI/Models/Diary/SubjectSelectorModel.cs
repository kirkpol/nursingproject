﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Diary
{
    public class SubjectSelectorModel
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string QRCode { get; set; }
        public string QRCodePath { get; set; }
        public string TeacherCreateId { get; set; }
        public string TeacherCreateName { get; set; }
        public string TeacherCreateType { get; set; }
        public List<TeacherAssistantSelectorModel> AssistantSelector { get; set; }
        public List<StudenSubjectDetailModel> studenSubjectDetail { get; set; }
    }

    public class TeacherAssistantSelectorModel
    {
        public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string TeacherType { get; set; }
    }

    public class StudenSubjectDetailModel
    {
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string TeacherId { get; set; }
    }
}

