﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Diary
{
    public class DiaryListModel
    {
        public int DiaryId { get; set; }
        public int StatusId { get; set; }
        public int SubjectId { get; set; }
        public string subjectName { get; set; }
        public string DiaryTitle { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string DateWard { get; set; }
    }
}
