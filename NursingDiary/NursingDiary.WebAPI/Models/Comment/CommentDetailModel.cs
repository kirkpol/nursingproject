﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NursingDiary.WebAPI.Models.Comment
{
    public class CommentDetailModel
    {
        public int CommentId { get; set; }
        public int DiaryId { get; set; }
        public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string CommentContent { get; set; }
        public string DateComment { get; set; }
    }
}
