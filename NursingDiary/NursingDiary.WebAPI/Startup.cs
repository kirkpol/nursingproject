﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NursingDiary.DataAccess.EntityFramework.Implement;
using NursingDiary.DataAccess.EntityFramework.Interface;
using NursingDiary.Logic.Implement;
using NursingDiary.Logic.Interface;
using Swashbuckle.AspNetCore.Swagger;

namespace NursingDiary.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //add swagger interactive documentation
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Info { Title = "API", Version = "v1" });

               
            });

            services.AddCors(options =>
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder.AllowAnyOrigin();
                        builder.AllowAnyHeader();
                        builder.AllowAnyMethod();
                        builder.AllowCredentials();
                        builder.WithOrigins("http://localhost:4200");
                    })
                );

            services.AddScoped<IEntityFrameworkContext, EntityFrameworkContext>();
            services.AddScoped<IEntityUnitOfWork, EntityUnitOfWork>();
            services.AddScoped<ILogicUnitOfWork, LogicUnitOfWork>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                const string Url = "/swagger/v1/swagger.json";
                config.SwaggerEndpoint(Url, "API V1");
            });
            app.UseCors("AllowAll");
        }
    }
}
