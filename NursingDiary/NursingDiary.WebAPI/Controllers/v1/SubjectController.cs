﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Subject;
using NursingDiary.WebAPI.Models.Diary;
using NursingDiary.WebAPI.Models.Subject;

namespace NursingDiary.WebAPI.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class SubjectController : Controller
    {
        private ILogicUnitOfWork LogicUnitOfWork { get; set; }

        public SubjectController(ILogicUnitOfWork LogicUnitOfWork)
        {
            this.LogicUnitOfWork = LogicUnitOfWork;
        }


        [HttpPost("CreateSubject")]
        public async Task<object> CreateSubject(SubjectModel data)
        {
            SubjectCreateDto subjectCreateDto = new SubjectCreateDto()
            {

                SubjectName = data.SubjectName,
                TeacherId = data.TeacherId,
                TeacherAssistants = data.TeacherAssistants != null ? data.TeacherAssistants.Select(s => new TeacherAssistantDto()
                {
                    TeacherId = s.TeacherId
                }).ToList() : null
            };

            var res = await LogicUnitOfWork.SubjectService.CreateSubjectAsync(subjectCreateDto);

            var result = new SubjectDetailModel()
            {            
                SubjectName = res.SubjectName,
                SubjectId = res.SubjectId,
                QRCode = res.QRCode,
                QRCodePath = res.QRCodePath
            };
            return new OkObjectResult(result);
        }

        [HttpPost("AddSubject")]
        public async Task<object> AddSubject(AddSubjectModel model)
        {
            string studentId = model.StudentId;
            string teacherId = model.TeacherId;
            string qrcode = model.Qrcode;
            var res = await LogicUnitOfWork.SubjectService.AddSubjectAsync(studentId, teacherId, qrcode);

            //var result = new SubjectSingleDetailModel()
            //{
            //    SubjectName = res.SubjectName,
            //    TeacherHeadId = res.TeacherHeadId,
            //    TeacherHeadName = res.TeacherHeadName,
            //    TeacherAssistantId = res.TeacherAssistantId,
            //    TeacherAssistantName = res.TeacherAssistantName,
            //    QRCode = res.QRCode
            //};
            return new OkObjectResult(res);
        }

        [HttpPost("SubjectTeacherList")]
        public async Task<object> SubjectTeacherList(SubjectTeacherPara model)
        {
            string teacherId = model.TeacherId;
            var res = await LogicUnitOfWork.SubjectService.SubjectTeacherListAsync(teacherId);

            var result = res.Select(s => new SubjectIndexListModel()
            {
                SubjectId = s.SubjectId,
                SubjectName = s.SubjectName,
                StudentId = s.StudentId,
                TeacherId = s.TeacherId,
                TeacherName = s.TeacherName,
                TeacherType = s.TeacherType
            }).ToList();

            return new OkObjectResult(result);
        }

        [HttpPost("SubjectStudentList")]
        public async Task<object> SubjectStudentList(SubjectStudentPara model)
        {
            string studentId = model.StudentId;
            var res = await LogicUnitOfWork.SubjectService.SubjectStudentListAsync(studentId);

            var result = res.Select(s => new SubjectIndexListModel()
            {
                SubjectId = s.SubjectId,
                SubjectName = s.SubjectName,
                StudentId = s.StudentId,
                TeacherId = s.TeacherId,
                TeacherName = s.TeacherName,
                TeacherType = s.TeacherType
            }).ToList();

            return new OkObjectResult(result);
        }

        [HttpPost("SubjectSelector")]
        public async Task<object> SubjectSelector(SubjectParaModel model)
        {
            string qrCode = model.QRCode;
            var res = await LogicUnitOfWork.SubjectService.SubjectSelectorAsync(qrCode);

            var result = new SubjectSelectorModel()
            {
                SubjectId = res.SubjectId,
                SubjectName = res.SubjectName,
                QRCode = res.QRCode,
                QRCodePath = res.QRCodePath,
                TeacherCreateId = res.TeacherCreateId,
                TeacherCreateName = res.TeacherCreateName,
                TeacherCreateType = res.TeacherCreateType,
                AssistantSelector = res.AssistantSelector.Select(s => new TeacherAssistantSelectorModel()
                {
                    TeacherId = s.TeacherId,
                    TeacherName = s.TeacherName,
                    TeacherType = s.TeacherType
                }).ToList()
            };

            return new OkObjectResult(result);
        }

        [HttpPost("SubjectDetail")]
        public async Task<object> SubjectDetail(SubjectParaModel model)
        {
            int subjectId = model.SubjectId;
            var res = await LogicUnitOfWork.SubjectService.SubjectDetailAsync(subjectId);

            var result = new SubjectSelectorModel()
            {
                SubjectId = res.SubjectId,
                SubjectName = res.SubjectName,
                QRCode = res.QRCode,
                QRCodePath = res.QRCodePath,
                TeacherCreateId = res.TeacherCreateId,
                TeacherCreateName = res.TeacherCreateName,
                TeacherCreateType = res.TeacherCreateType,
                AssistantSelector = res.AssistantSelector.Select(s => new TeacherAssistantSelectorModel()
                {
                    TeacherId = s.TeacherId,
                    TeacherName = s.TeacherName,
                    TeacherType = s.TeacherType
                }).ToList(),
                studenSubjectDetail = res.StudenSubjectDetail.Select( s => new StudenSubjectDetailModel()
                {
                    StudentId = s.StudentId,
                    StudentName = s.StudentName,
                    TeacherId = s.TeacherId
                }).ToList()
            };

            return new OkObjectResult(result);
        }
    }
}