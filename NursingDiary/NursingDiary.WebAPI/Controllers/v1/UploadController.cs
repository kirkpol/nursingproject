﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Upload;
using NursingDiary.WebAPI.Models.User;

namespace NursingDiary.WebAPI.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class UploadController : ControllerBase
    {
        private ILogicUnitOfWork LogicUnitOfWork { get; set; }

        public UploadController(ILogicUnitOfWork LogicUnitOfWork)
        {
            this.LogicUnitOfWork = LogicUnitOfWork;
        }

        [HttpPost("Upload")]
        public async Task<object> UploadImage(IFormFile image)
        {
            var result = await LogicUnitOfWork.UploadService.UploadObjectAsync(image);
            UploadPhotoModel upload = new UploadPhotoModel()
            {
                FileName = result.FileName,
                Success = result.Success
                
            };
            return new ObjectResult(upload);
        }

        [HttpPost("Excel")]
        
        public async Task<object> ImportExcel(IFormFile file ,int userType)
        {
            
            var result = await LogicUnitOfWork.UploadService.ImportExcelFileAsync(file, userType);           
            return new ObjectResult(result);
        }
    }
}
