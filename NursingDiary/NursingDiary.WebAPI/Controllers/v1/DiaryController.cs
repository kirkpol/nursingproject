﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Diary;
using NursingDiary.WebAPI.Models.Diary;
using static NursingDiary.WebAPI.Models.Diary.DiaryDetailModel;

namespace NursingDiary.WebAPI.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class DiaryController : Controller
    {
        private ILogicUnitOfWork LogicUnitOfWork { get; set; }

        public DiaryController(ILogicUnitOfWork LogicUnitOfWork)
        {
            this.LogicUnitOfWork = LogicUnitOfWork;
        }
        [HttpPost("RecordDiary")]
        public async Task<object> RecordDiary(DiaryCreateModel model)
        {
            var imageRes = await LogicUnitOfWork.UploadService.UploadObjectAsync(model.UploadImage);
            DiaryPictureCreateDto imageSuccess = new DiaryPictureCreateDto()
            {
                PicName = imageRes.FileName
            }; 
            DiaryCreateDto diaryCreateDto = new DiaryCreateDto()
            {
                DiaryTitle = model.DiaryTitle,
                DiaryContent = model.DiaryContent,
                DateCreate = model.DateCreate,
                DateWard = model.DateWard,
                SubjectId = model.SubjectId,
                StudentId = model.StudentId,
                PictureList = imageSuccess,
                //PictureList = model.PictureList != null ? model.PictureList.Select(s => new DiaryPictureCreateDto()
                //{
                //    PicName = s.PicName
                //}).ToList() : null,
                HashTagList = model.HashTagList != null ? model.HashTagList.Select(s => new DiaryHashTagCrateDto()
                {
                    HashTagName = s.HashTagName
                }).ToList() : null           
            };

            var res = await LogicUnitOfWork.DiaryService.RecordDiaryAsync(diaryCreateDto);

            /*var result = new DiaryDetailModel()
            {
                DiaryId = res.DiaryId,
                DiaryTitle = res.DiaryTitle,
                PictureCount = res.PictureCount
            };*/
            return new OkObjectResult(res);
        }

        [HttpPost("SuccessDiary")]
        public async Task<object> SuccessDiary(DiaryDetailParaModel model)
        {
            int diaryId = model.DiaryId;
            var res = await LogicUnitOfWork.DiaryService.SuccessDiaryAsyncs(diaryId);
            return new OkObjectResult(res);
        }

        [HttpPost("DiaryDetail")]
        public async Task<object> DiaryDetail (DiaryDetailParaModel model)
        {
            int diaryid = model.DiaryId;
            var res = await LogicUnitOfWork.DiaryService.DiaryDetailAsync(diaryid);
            var result = new DiaryDetailModel() {
                DiaryId = res.DiaryId,
                DiaryTitle = res.DiaryTitle,
                DiaryContent = res.DiaryContent,
                DateCreate = res.DateCreate,
                DateWard = res.DateWard,
                DateCreateToString = res.DateCreateToString,
                DateWardToString = res.DateWardToString,
                StatusId = res.StatusId,
                StudentId = res.StudentId,
                SubjectId = res.SubjectId,
                SubjectName = res.SubjectName,
                TeacherId = res.TeacherId,
                diaryHashtagDetails = res.diaryHashtagDetails.Select(s => new DiaryHashTagModel()
                {
                    HashTagId = s.HashTagId,
                    HashTagName =s.HashTagName
                }).ToList(),
                diaryPictureDetials = res.diaryPictureDetials.Select(s => new DiaryPictureModel()
                {
                    PicId = s.PicId,
                    PicName =s.PicName
                }).ToList()                          
            };
            return new OkObjectResult(result);
        }

        [HttpPost("DiaryList")]
        public async Task<object>DirayList(DiarySingleParaModel model)
        {
            string studentId = model.StudentId;
            var res = await LogicUnitOfWork.DiaryService.GetAllDiaryListAsync(studentId);
            var result = res.Select(s => new DiaryListModel()
            {
                DiaryId = s.DiaryId,
                DiaryTitle = s.DiaryTitle,
                StatusId = s.StatusId,
                SubjectId = s.SubjectId,
                subjectName = s.subjectname,
                StudentId = s.StudentId,
                StudentName = s.StudentName,
                DateWard = s.DateWard
            }).ToList();

            return new OkObjectResult(result);

        }

        [HttpPost("DiaryTeacherList")]
        public async Task<object> DiaryTeacherList(DiaryParaModel model)
        {
            DiaryParaDto data = new DiaryParaDto();
            data.StudentId = model.StudentId;
            data.SubjectId = model.SubjectId;
            data.TeacherId = model.TeacherId;

            var res = await LogicUnitOfWork.DiaryService.DiaryTeacherListAsync(data);

            var result = res.Select(s => new DiaryListModel()
            {
                DiaryId = s.DiaryId,
                DiaryTitle =s.DiaryTitle,
                StatusId = s.StatusId,
                SubjectId = s.SubjectId,
                StudentId =s.StudentId,
                StudentName = s.StudentName,
                DateWard = s.DateWard          
            }).ToList();

            return new OkObjectResult(result);
        }

        [HttpPost("DiaryStudentList")]
        public async Task<object> DiaryStudentList(DiaryParaModel model)
        {
            DiaryParaDto data = new DiaryParaDto();
            data.StudentId = model.StudentId;
            data.SubjectId = model.SubjectId;
            data.TeacherId = model.TeacherId;

            var res = await LogicUnitOfWork.DiaryService.DiaryStudentListAsync(data);

            var result = res.Select(s => new DiaryListModel()
            {
                DiaryId = s.DiaryId,
                DiaryTitle = s.DiaryTitle,
                StatusId = s.StatusId,
                SubjectId = s.SubjectId,
                StudentId = s.StudentId,
                StudentName = s.StudentName,
                DateWard = s.DateWard
            }).ToList();

            return new OkObjectResult(result);
        }
    }
}