﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Comment;
using NursingDiary.WebAPI.Models.Comment;

namespace NursingDiary.WebAPI.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class CommentController : Controller
    {
        private ILogicUnitOfWork LogicUnitOfWork { get; set; }

        public CommentController(ILogicUnitOfWork LogicUnitOfWork)
        {
            this.LogicUnitOfWork = LogicUnitOfWork;
        }

        [HttpPost("CreateComment")]
        public async Task<object> CreateComment(CommentModel data)
        {
            var comment = new CommentDto(){
                CommentContent = data.CommentContent,
                DiaryId = data.DiaryId,
                TeacherId = data.TeacherId
            };
            var res = await LogicUnitOfWork.CommentService.CreateCommentAsync(comment);
            if (res != null)
            {
                var result = new CommentModel()
                {
                    CommentId = res.CommentId,
                    CommentContent = res.CommentContent,
                    DateComment = res.DateComment,
                    DiaryId = res.DiaryId,
                    TeacherId = res.TeacherId,
                    TeacherName = res.TeacherName
                };
                return new OkObjectResult(result);
            }
            else
            {
                return new OkObjectResult(null);
            }
        }

        [HttpPost("CommentDetail")]
        public async Task<object> CommentDetail(CommentParaModel model)
        {
            int diaryId = model.DiaryId;
            var res = await LogicUnitOfWork.CommentService.CommentDetailAsync(diaryId);
            var result = res.Select(s => new CommentDetailModel(){
                DiaryId = s.DiaryId,
                CommentId = s.CommentId,
                CommentContent = s.CommentContent,
                DateComment = s.DateComment,
                TeacherId = s.TeacherId,
                TeacherName = s.TeacherName
            }).ToList();
            return new OkObjectResult(result);
        }
    }
}