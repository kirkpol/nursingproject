﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Teacher;
using NursingDiary.WebAPI.Models.Teacher;

namespace NursingDiary.WebAPI.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class TeacherController : Controller
    {
        private ILogicUnitOfWork LogicUnitOfWork { get; set; }

        public TeacherController(ILogicUnitOfWork LogicUnitOfWork)
        {
            this.LogicUnitOfWork = LogicUnitOfWork;
        }
        
        [HttpPost("InsertTeacher")]
        public async Task<object> InsertTeacher(TeacherModel data)
        {
            TeacherDto teacherDto = new TeacherDto()
            {
                TeacherId = data.UserId,
                Password = data.Password,
                Firstname = data.Firstname,
                Lastname = data.Lastname,
            };
            var result = await LogicUnitOfWork.TeacherService.InsertTeacherAsync(teacherDto);
            return new OkObjectResult(result);
        }

        [HttpGet("GetTeacherDetail")]
        public async Task<object> GetTeacherDetail(string id)
        {          
            var res = await LogicUnitOfWork.TeacherService.GetTeacherDetailAsync(id);
            var result = new TeacherModel()
            {
                UserId = res.TeacherId,
                Password = res.Password,
                Firstname = res.Firstname,
                Lastname = res.Lastname,
                TypeId = res.TypeId
            };
            return new OkObjectResult(result);
        }

        [HttpGet("GetAllTeacher")]
        public async Task<object> GetAlltTeacher()
        {
            var res = await LogicUnitOfWork.TeacherService.GetAllTeacherAsync();
            var result = res.Select(s => new TeacherModel()
            {
                UserId = s.TeacherId,
                Password = s.Password,
                Firstname = s.Firstname,
                Lastname = s.Lastname,
                TypeId =  s.TypeId
                
            }).ToList();
           
            return new OkObjectResult(result);
        }

        [HttpDelete("DeleteTeacher")]
        public async Task<object> DeleteTeacher(string id)
        {
            var res = await LogicUnitOfWork.TeacherService.DeleteTeacherAsync(id);

            return new OkObjectResult(res);
        }

        [HttpPost("EditTeacher")]
        public async Task<object> EditTeacher(TeacherModel data)
        {
            TeacherDto teacherDto = new TeacherDto()
            {
                TeacherId = data.UserId,
                Password = data.Password,
                Firstname = data.Firstname,
                Lastname = data.Lastname,
            };
            var result = await LogicUnitOfWork.TeacherService.EditTeacherAsync(teacherDto);
            return new OkObjectResult(result);
        }

        [HttpPost("AttachFile")]
        public  bool AttachFile(IFormFile file)
        {
          
            TeacherModel teacher = new TeacherModel();
            return true;
        }
    }
}