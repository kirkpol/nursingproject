﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.User;

namespace NursingDiary.WebAPI.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class UserController : Controller
    {
        private ILogicUnitOfWork LogicUnitOfWork { get; set; }

        public UserController(ILogicUnitOfWork LogicUnitOfWork)
        {
            this.LogicUnitOfWork = LogicUnitOfWork;
        }

        [HttpPost("Login")]
        public async Task<object> Login(UserModel data)
        {
            string username = data.UserId;
            string password = data.Password;
            var res = await LogicUnitOfWork.UserService.LoginAsync(username, password);

            var userData = new UserModel();
            if (res != null)
            {
                userData.UserId = res.UserId;
                userData.Firstname = res.Firstname;
                userData.Lastname = res.Lastname;
                userData.TypeId = res.TypeId;
            }
            else
            {
                return new OkObjectResult(null);
            }
            
            return new OkObjectResult(userData);
        }
    }
}