﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Status;
using NursingDiary.WebAPI.Models.Status;

namespace NursingDiary.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class StatusController : Controller
    {
        private ILogicUnitOfWork LogicUnitOfWork { get; set; }

        public StatusController(ILogicUnitOfWork LogicUnitOfWork)
        {
            this.LogicUnitOfWork = LogicUnitOfWork;
        }

        [HttpPost("InsertStatus")]
        public async Task<object>CreateStatus(StatusModel model)
        {
            StatusDto statusDto = new StatusDto()
            {
                StatusType = model.StatusType,
                StatusName = model.StatusName
            };
            var result = await LogicUnitOfWork.StatusService.CreateStatusAsync(statusDto);
            return new OkObjectResult(result);
        }
    }
}