﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NursingDiary.Logic.Interface;
using NursingDiary.Logic.Models.Student;
using NursingDiary.WebAPI.Models.Student;

namespace NursingDiary.WebAPI.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class StudentController : Controller
    {
        private ILogicUnitOfWork LogicUnitOfWork { get; set; }

        public StudentController(ILogicUnitOfWork LogicUnitOfWork)
        {
            this.LogicUnitOfWork = LogicUnitOfWork;
        }

        [HttpPost("InsertStudent")]
        public async Task<object> InsertStudent(StudentModel data)
        {
            StudentDto studentDto = new StudentDto()
            {
                StudentId = data.UserId,
                Password = data.Password,
                Firstname = data.Firstname,
                Lastname = data.Lastname,
            };
            var result = await LogicUnitOfWork.StudentService.InsertStudentAsync(studentDto);
            return new OkObjectResult(result);
        }

        [HttpGet("GetStudentDetail")]
        public async Task<object> GetStudentDetail(string id)
        {
            var res = await LogicUnitOfWork.StudentService.GetStudentDetailAsync(id);
            var result = new StudentModel()
            {
                UserId = res.StudentId,
                Password = res.Password,
                Firstname = res.Firstname,
                Lastname = res.Lastname,
                TypeId = res.TypeId
            };
            return new OkObjectResult(result);
        }

        [HttpGet("GetAlltStudent")]
        public async Task<object> GetAllStudent()
        {
            var res = await LogicUnitOfWork.StudentService.GetAllStudentAsync();
            var result = res.Select(s => new StudentModel()
            {
                UserId = s.StudentId,
                Password = s.Password,
                Firstname = s.Firstname,
                Lastname = s.Lastname,
                Nickname = s.Nickname,
                TypeId = s.TypeId
            }).ToList();

            return new OkObjectResult(result);
        }

        [HttpDelete("DeleteStudent")]
        public async Task<object> DeleteStudent(string id )
            {
            var res = await LogicUnitOfWork.StudentService.DeleteStudentAsync(id);
           
            return new OkObjectResult(res);
        }

        [HttpPost("EditStudent")]
        public async Task<object> EditStudent(StudentModel data)
        {
            StudentDto studentDto = new StudentDto()
            {
                StudentId = data.UserId,
                Password = data.Password,
                Firstname = data.Firstname,
                Lastname = data.Lastname,
            };
            var result = await LogicUnitOfWork.StudentService.EditStudentAsync(studentDto);
            return new OkObjectResult(result);
        }
    }
}